import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { IonicStorageModule } from '@ionic/storage-angular';
import { Drivers, Storage } from '@ionic/storage';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { IonicSelectableModule } from 'ionic-selectable';
import { QRCodeModule } from 'angularx-qrcode';
import { FontPickerModule } from 'ngx-font-picker';
import { FONT_PICKER_CONFIG } from 'ngx-font-picker';
import { FontPickerConfigInterface } from 'ngx-font-picker';
// import { PDFGenerator } from '@awesome-cordova-plugins/pdf-generator';
import { StatusBar } from '@awesome-cordova-plugins/status-bar/ngx';
import { SplashScreen } from '@awesome-cordova-plugins/splash-screen/ngx';
import { LocalNotifications } from '@awesome-cordova-plugins/local-notifications/ngx';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';

const DEFAULT_FONT_PICKER_CONFIG: FontPickerConfigInterface = {
  // Change this to your Google API key
  apiKey: 'AIzaSyBSHGOL2K-S18rmpaM_YPcrhPrx6_PByHg'
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot({
      mode: 'ios',
      backButtonText: ''
    }),
    AppRoutingModule, 
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MaterialModule,
    IonicStorageModule.forRoot({
      driverOrder: [Drivers.SecureStorage, Drivers.IndexedDB, Drivers.LocalStorage]
    }),
    NgxMaterialTimepickerModule,
    IonicSelectableModule,
    QRCodeModule,
    FontPickerModule,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    // PDFGenerator,
    LocalNotifications,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: FONT_PICKER_CONFIG,
      useValue: DEFAULT_FONT_PICKER_CONFIG
    },
    InAppBrowser
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
