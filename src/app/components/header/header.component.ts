import { Component, Input, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() title: null;
  @Input() data_type: string;
  @Input() create_btn: boolean = false;



  constructor(
    public modalService: ModalService
  ) { }

  ngOnInit() {
    console.log(this.data_type);
  }

}
