import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Router, RoutesRecognized } from '@angular/router';
import { Subscription } from 'rxjs';
import { distinctUntilChanged, filter, map, pairwise, tap } from 'rxjs/operators';
import { data_type, Customer, Partner, Appointment, Question, Order, Design_Order } from 'src/app/interface';
import { ModalService } from 'src/app/services/modal.service';
import { Data_Type_Mapping_Local_Storage, StorageService } from 'src/app/services/storage.service';
import { UtilService } from 'src/app/services/util.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AlertController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { NavigationOptions } from '@ionic/angular/providers/nav-controller';
import { StateService } from 'src/app/services/state.service';
import { CsvService } from 'src/app/services/csv.service';
import { EmailService } from 'src/app/services/email.service';

const customer_data_displayed_cols: string[] = ['delete', 'username', 'display_name', 'email', 'is_vip', "create_datetime", 'action'];
const partner_data_displayed_cols: string[] = ['delete', 'username', 'display_name', 'email', 'is_staff', "create_datetime", 'sales', 'action'];
const reservation_timeslot_data_displayed_cols: string[] = ['delete', 'date', 'start_time', 'end_time', 'quota', "price", "create_datetime", 'action'];
const appointment_data_displayed_cols: string[] = ['delete', 'reservation_datetime', 'status', "customer", 'partner', "create_datetime", 'action'];
const banner_data_displayed_cols: string[] = ['delete', 'image', 'status', "create_datetime", 'action'];
const question_data_displayed_cols: string[] = ['delete', 'status', "title", "description", "response", "create_datetime", 'action'];
const notificatino_data_displayed_cols: string[] = ['delete', 'status', "title", "content", "create_datetime", 'action'];
const product_data_displayed_cols: string[] = ['delete', 'image', 'status', 'redeemable', 'credit', 'category', "name", "price", 'vip_price', "inventory", "create_datetime", 'action'];
const order_data_displayed_cols: string[] = ['delete', 'status', 'customer_data', "cart", 'total_amount', "create_datetime", 'action'];
const design_order_data_displayed_cols: string[] = ['delete', 'status', 'design_product', "total_amount", 'commission', "create_datetime", 'action'];

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  current_page = null;
  displayedColumns: string[] = null;
  display_data: any[] = null;
  dataSource = new MatTableDataSource(null);
  dataSub$$: Subscription = null;

  @Input() data_type = null;
  @ViewChild(MatTable, { static: false }) table: MatTable<any>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    public router: Router,
    public util: UtilService,
    public modalService: ModalService,
    private changeDetectorRefs: ChangeDetectorRef,
    private storageService: StorageService,
    private cdr: ChangeDetectorRef,
    private alertController: AlertController,
    private authService: AuthService,
    private nav: NavController,
    private state: StateService,
    private csvService: CsvService,
    public emailService: EmailService
  ) {
    // this.router.events
    // .pipe(filter((evt: any) => evt instanceof RoutesRecognized), pairwise())
    // .subscribe((events: RoutesRecognized[]) => {
    //   // console.log('previous url', events[0].urlAfterRedirects);
    //   // console.log('current url', events[1].urlAfterRedirects);

    //   this.current_page = events[1].urlAfterRedirects;
    //   console.log(this.current_page);

    //   // let previous_page = events[0].urlAfterRedirects;
    // });

  }

  ngOnInit() {
    
    // console.log(this.dataSource);

    this.current_page = this.router.url.replace('/', '');
    switch (this.current_page) {
      case 'customer':
        this.dataSub$$ = this.storageService.LOCAL_STORE.customer_data_list
          // .pipe(distinctUntilChanged())
          .subscribe(data => {
            this.display_data = data;
            this.initTable(data);
          });
        this.displayedColumns = customer_data_displayed_cols;
        break;
      case 'partner':
        this.dataSub$$ = this.storageService.LOCAL_STORE.partner_data_list
          // .pipe(distinctUntilChanged())
          .subscribe(data => {
            this.display_data = data;
            this.initTable(data);
          });
        this.displayedColumns = partner_data_displayed_cols;
        break;
      case 'reservation':
        this.dataSub$$ = this.storageService.LOCAL_STORE.reservation_timeslot_data_list
          // .pipe(distinctUntilChanged())
          .subscribe(data => {
            this.display_data = data;
            this.initTable(data);
          });
        this.displayedColumns = reservation_timeslot_data_displayed_cols;
        break;
      case 'appointment':
        this.dataSub$$ = this.storageService.LOCAL_STORE.appointment_data_list
          .pipe(
            // distinctUntilChanged(),
            map((d) => {
              if (d != null) {
                console.log(d);
                d = d.map((a: Appointment) => {
                  a['reservation_timeslot_data'] = this.storageService.LOCAL_STORE.reservation_timeslot_data_list.value.find(r => r.id == a.reservation_timeslot_id);
                  a['customer_data'] = this.storageService.LOCAL_STORE.customer_data_list.value.find(r => r.id == a.customer_id);
                  a['partner_data'] = this.storageService.LOCAL_STORE.partner_data_list.value.find(r => r.id == a.partner_id);
                  return a;
                });
                // d['reservation_timeslot_data'] = this.storageService.LOCAL_STORE.reservation_timeslot_data_list.value.find(r => r.id == d.reservation_timeslot_id);
              }

              return d;
            })
          )
          .subscribe(data => {
            this.display_data = data;
            this.initTable(data);
          });
        this.displayedColumns = appointment_data_displayed_cols;
        break;

      case 'banner':
        this.dataSub$$ = this.storageService.LOCAL_STORE.banner_data_list
          // .pipe(distinctUntilChanged())
          .subscribe(data => {
            this.display_data = data;
            this.initTable(data);
          });
        this.displayedColumns = banner_data_displayed_cols;
        break;

      case 'question':
        this.dataSub$$ = this.storageService.LOCAL_STORE.question_data_list
          // .pipe(distinctUntilChanged())
          .pipe(
            // distinctUntilChanged(),
            map((d) => {
              if (d != null) {
                // console.log(d);
                d = d.map((q: Question) => {
                  if (Array.isArray(this.storageService.LOCAL_STORE.response_data_list.value)) {
                    q['no_of_response'] = this.storageService.LOCAL_STORE.response_data_list.value.filter(r => r.question_id == q.id).length;
                  }
                  return q;
                });
                // d['reservation_timeslot_data'] = this.storageService.LOCAL_STORE.reservation_timeslot_data_list.value.find(r => r.id == d.reservation_timeslot_id);
              }

              return d;
            })
          )
          .subscribe(data => {
            this.display_data = data;
            this.initTable(data);
          });
        this.displayedColumns = question_data_displayed_cols;
        break;

      case 'notification':
        this.dataSub$$ = this.storageService.LOCAL_STORE.notification_data_list
          // .pipe(distinctUntilChanged())
          .subscribe(data => {
            this.display_data = data;
            this.initTable(data);
          });
        this.displayedColumns = notificatino_data_displayed_cols;
        break;

      case 'product':
        this.dataSub$$ = this.storageService.LOCAL_STORE.product_data_list
          // .pipe(distinctUntilChanged())
          .subscribe(data => {
            this.display_data = data;
            this.initTable(data);
          });
        this.displayedColumns = product_data_displayed_cols;
        break;

      case 'order':
        this.dataSub$$ = this.storageService.LOCAL_STORE.order_data_list
          // .pipe(distinctUntilChanged())
          .pipe(
            // distinctUntilChanged(),
            map((d) => {
              if (d != null) {
                // console.log(d);
                d = d.filter((o: Order) => o.is_redeemption == false)
                d.map((o: Order) => {
                  o['customer_data'] = this.storageService.LOCAL_STORE.customer_data_list.value ? this.storageService.LOCAL_STORE.customer_data_list.value.find((c: Customer) => c.id == o.customer_id) : null;
                  return o;
                });
                // d['reservation_timeslot_data'] = this.storageService.LOCAL_STORE.reservation_timeslot_data_list.value.find(r => r.id == d.reservation_timeslot_id);
              }
              return d;
            })
          )
          .subscribe(data => {
            this.display_data = data;
            this.initTable(data);
            console.log(data);
          });
        this.displayedColumns = order_data_displayed_cols;
        break;

      case 'redeem':
        this.dataSub$$ = this.storageService.LOCAL_STORE.order_data_list
          // .pipe(distinctUntilChanged())
          .pipe(
            // distinctUntilChanged(),
            map((d) => {
              if (d != null) {
                // console.log(d);
                d = d.filter((o: Order) => o.is_redeemption == true)
                d.map((o: Order) => {
                  o['customer_data'] = this.storageService.LOCAL_STORE.customer_data_list.value ? this.storageService.LOCAL_STORE.customer_data_list.value.find((c: Customer) => c.id == o.customer_id) : null;
                  return o;
                });
                // d['reservation_timeslot_data'] = this.storageService.LOCAL_STORE.reservation_timeslot_data_list.value.find(r => r.id == d.reservation_timeslot_id);
              }
              return d;
            })
          )
          .subscribe(data => {
            this.display_data = data;
            this.initTable(data);
          });
        this.displayedColumns = order_data_displayed_cols;
        break;

      case 'design-order':
        this.dataSub$$ = this.storageService.LOCAL_STORE.design_order_data_list
          // .pipe(distinctUntilChanged())
          .pipe(
            // distinctUntilChanged(),
            map((d) => {
              if (d != null) {
                // console.log(d);
                // d.map((o: Design_Order) => {
                //   o['customer_data'] = this.storageService.LOCAL_STORE.customer_data_list.value ? this.storageService.LOCAL_STORE.customer_data_list.value.find((c: Customer) => c.id == o.customer_id) : null;
                //   return o;
                // });
              }
              return d;
            })
          )
          .subscribe(data => {
            this.display_data = data;
            this.initTable(data);
          });
        this.displayedColumns = design_order_data_displayed_cols;
        break;

      default:
        break;
    }
  }

  initTable(data) {
    this.dataSource = new MatTableDataSource(data);
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.cdr.detectChanges();
    }, 300);

  }

  ngOnDestory() {
    this.dataSub$$.unsubscribe();
  }



  async deleteDataRequest(data) {
    const confirmed = await this.util.deleteDataAlert();
    if (confirmed) {
      this.deleteData(data);
    }
  }


  deleteData(data) {
    let send_data = {
      id: data.id,
      data_type: data.data_type
    }
    // this.isLoading = true;
    // this.httpService.DeleteDataByDataTypeAndId(send_data).then(res => {
    //   console.log(res);
    //   this.isLoading = false;
    //   if (res.result == "success") {
    //     window.history.back();
    //   }
    //   else {
    //     this.openSnackBar("刪除資料失敗！");
    //   }
    // });

    let data_list: any[] = this.storageService.LOCAL_STORE[Data_Type_Mapping_Local_Storage[data.data_type]].value;
    // console.log(data_list);
    // console.log(data.id);
    let index = data_list.findIndex(d => d.id == data.id);
    // console.log(index);
    if (index != -1) {
      data_list.splice(index, 1);
      this.storageService.LOCAL_STORE[Data_Type_Mapping_Local_Storage[data.data_type]].next(data_list);
      this.util.openSnackBar("資料已被刪除！");
    }
    else {
      this.util.openSnackBar("刪除資料失敗！");
    }
  }

  applyFilter(event: Event) {
    // console.log(event);
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  async presentResetPartnerPasswordAlert(partner_data: Partner) {
    const alert = await this.alertController.create({
      header: 'Are you sure to reset password?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => { }
        },
        {
          text: 'Yes',
          role: 'confirm',
          handler: () => {
            this.authService.resetPartnerPassword(partner_data);
          }
        }
      ]
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    if (role == 'comfirm'){

    }
  }

  async presentCustomNotificationAlert(data) {
    let msg = '';
    if (data.data_type == data_type.appointment_data){
      msg = `你的預約狀態為 － ${data.status}`;
    }
    else if (data.data_type == data_type.order_data){
      msg = `你的訂單狀態為 － ${data.status}`;
    }
    const alert = await this.alertController.create({
      header: 'Please confirm below notification content',
      buttons: [
        {
          "text": 'Send',
          handler: () => {
            this.util.openSnackBar("通知已傳送！");
          }
        }
      ],
      inputs: [
        {
          type: 'textarea',
          value: msg,
          placeholder: 'Notification content'
        }
      ]
    });

    await alert.present();
    
  }

  downloadOrderReport(){
    let data_to_export = [];

    this.display_data.forEach(data => {
      data_to_export.push(
        {
          "create_datetime": data.create_datetime,
          "status": data.status,
          "customer_phone": data.customer_data.phone,
          "customer_email": data.customer_data.email,
          "quantity": data.cart.map(d => d.quantity).reduce(function (a, b) { return a + b; }),
          "total_amount": data.total_amount,
          "total_credit": data.total_credit
        }
      );
    });
    console.log(data_to_export);
    // return;
    this.csvService.downloadFile(data_to_export, 'order-sales-summary', `report_${new Date().getTime()}`);
  }

  downloadDesignOrderReport(){
    let data_to_export = [];

    this.display_data.forEach(data => {
      data_to_export.push(
        {
          "Create datetime": data.create_datetime,
          "Status": data.status,
          "Customer phone": data.customer_phone,
          "Customer name": data.customer_name,
          "Customer email": data.customer_email,
          "Product type": data.product_type == 'tshirt' ? 'T-shirt' : 'Phone case',
          "Product attribute": data.design_product.attribute,
          "Product detail": data.product_detail,
          "Quantity": data.quantity,
          "Total amount": data.total_amount,
          "Commission": data.commission
        }
      );
    });
    console.log(data_to_export);
    // return;
    this.csvService.downloadFile(data_to_export, 'order-sales-summary', `report_${new Date().getTime()}`);
  }

  downloadPartnerAccPerformanceReport(){
    let data_to_export = [];

    let year_month = new Date(new Date().getFullYear(), new Date().getMonth(), 15).toISOString().substring(0,7);

    let monthly_data = this.display_data.filter( (p: Partner) => new Date(p.create_datetime).toISOString().substring(0,7) == year_month);

    let partner_list = monthly_data.map(d => d.partner_id);
    console.log(partner_list);
    
    partner_list.forEach(partner_id => {
      data_to_export.push({
        'Partner phone': this.storageService.LOCAL_STORE.partner_data_list.value.find(d => d.id == partner_id).phone,
        'Partner email': this.storageService.LOCAL_STORE.partner_data_list.value.find(d => d.id == partner_id).email,
        'Total quantity': monthly_data.filter(d => d.partner_id == partner_id).map(d => d.quantity).reduce(function (a, b) { return a + b; }),
        'Total sales': monthly_data.filter(d => d.partner_id == partner_id).map(d => d.total_amount).reduce(function (a, b) { return a + b; }),
        'Total commission': monthly_data.filter(d => d.partner_id == partner_id).map(d => d.commission).reduce(function (a, b) { return a + b; })
      })
    });
    
    console.log(data_to_export);

    this.csvService.downloadFile(data_to_export, 'monthly-partner-performance', `report_${new Date().getTime()}`);
  }
  

  downloadPartnerCommissionReport(){
    let data_to_export = [];

    let year_month = new Date(new Date().getFullYear(), new Date().getMonth(), 15).toISOString().substring(0,7);

    let monthly_data = this.display_data.filter( (p: Partner) => new Date(p.create_datetime).toISOString().substring(0,7) == year_month);

    let partner_list = monthly_data.map(d => d.partner_id);
    console.log(partner_list);
    
    partner_list.forEach(partner_id => {
      data_to_export.push({
        'Partner phone': this.storageService.LOCAL_STORE.partner_data_list.value.find(d => d.id == partner_id).phone,
        'Partner email': this.storageService.LOCAL_STORE.partner_data_list.value.find(d => d.id == partner_id).email,
        'Total commission': monthly_data.filter(d => d.partner_id == partner_id).map(d => d.commission).reduce(function (a, b) { return a + b; })
      })
    });
    
    console.log(data_to_export);

    this.csvService.downloadFile(data_to_export, 'monthly-commission', `report_${new Date().getTime()}`);
  }
  

  goSalesSummary(partner: Partner){
    this.state.sales_summary_partner.next(partner);
    this.nav.navigateRoot('sales-summary');
  }

  goActivityLog(partner: Partner){
    this.state.activity_log_partner.next(partner);
    this.nav.navigateRoot('activity-log');
  }

}
