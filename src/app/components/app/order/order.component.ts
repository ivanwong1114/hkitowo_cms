import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Order_Status } from 'src/app/interface';
import { AuthService } from 'src/app/services/auth.service';
import { OrderService } from 'src/app/services/order.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class OrderComponent implements OnInit {

  @Input() order_data = null;
  @Input() allow_edit = false;

  isCMS = environment.isCMS;

  user_data = this.auth.userData.value;

  public get OrderStatus(): typeof Order_Status {
    return Order_Status;
  }
  constructor(
    private orderService: OrderService,
    private cdf: ChangeDetectorRef,
    private auth: AuthService
  ) { }

  ngOnInit() {}

  changeQuantity(product, count){
    this.orderService.addOrRemoveProductFromOrder(product, count, this.order_data.is_redeemption);
    this.cdf.detectChanges();
  }
}
