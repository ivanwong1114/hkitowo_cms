import { Component, Input, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Product } from 'src/app/interface';
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/services/data.service';
import { StateService } from 'src/app/services/state.service';
import { StorageService } from 'src/app/services/storage.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {

  product_list = null;
  display_product_list = null;
  filterValue = null;

  user_data = this.auth.userData.value;

  @Input() is_redeem: boolean = null;

  constructor(
    private storageService: StorageService,
    private dataService: DataService,
    private util: UtilService,
    private nav: NavController,
    private state: StateService,
    private auth: AuthService
  ) { }

  ngOnInit() {

    setTimeout(() => {
      if (this.is_redeem){
        this.product_list = this.storageService.LOCAL_STORE.product_data_list.value ? this.storageService.LOCAL_STORE.product_data_list.value.filter((p: Product) => p.redeemable) : this.storageService.LOCAL_STORE.product_data_list.value;
        this.display_product_list = this.product_list;
        console.log(this.storageService.LOCAL_STORE.product_data_list.value);
      }
      else{
        this.product_list = this.storageService.LOCAL_STORE.product_data_list.value ? this.storageService.LOCAL_STORE.product_data_list.value.filter((p: Product) => !p.redeemable) : this.storageService.LOCAL_STORE.product_data_list.value;
        this.display_product_list = this.product_list;
        console.log(this.storageService.LOCAL_STORE.product_data_list.value);
      }
    }, 1000);
  }


  onSearchChange($event){
    this.display_product_list = null;
    this.filterValue = $event.detail.value.trim().toLowerCase();
    if (this.filterValue != null && this.filterValue != ''){
      this.display_product_list = this.product_list.filter((p: Product) => 
        p.name.trim().toLowerCase().includes(this.filterValue) ||
        p.description.trim().toLowerCase().includes(this.filterValue)
      );
    }
    else{
      this.display_product_list = this.product_list;
    }
  }

  goProductDetailPage(product: Product){
    this.state.current_product$.next(product);
    this.nav.navigateForward('product-detail', {
      state: {
        is_redeem: this.is_redeem
      }
    });
  }


}
