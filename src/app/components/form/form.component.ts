import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Appointment, Appointment_Status, Banner, Customer, data_type, General_Status, Notification, Order_Status, Partner, Product, Question, Reservation_Timeslot } from 'src/app/interface';
import { HttpService } from 'src/app/services/http.service';
import { ModalService } from 'src/app/services/modal.service';
import { Data_Type_Mapping_Local_Storage, StorageService } from 'src/app/services/storage.service';
import { UtilService } from 'src/app/services/util.service';
import { IonicSelectableComponent } from 'ionic-selectable';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  @Input() type: 'edit' | 'read' | 'create';
  @Input() data: any;
  @Input() data_type: data_type;

  general_status: any[] = Object.values(General_Status);
  order_status: any[] = Object.values(Order_Status);
  appointment_status: any[] = Object.values(Appointment_Status);

  customer_data_list = this.storageService.LOCAL_STORE.customer_data_list.value;
  partner_data_list = this.storageService.LOCAL_STORE.partner_data_list.value;
  ava_reservation_timeslot_data_list = null;
  customer_data_list_for_notification = this.storageService.LOCAL_STORE.customer_data_list.value.filter(d => d.status != General_Status.deleted && d.status != General_Status.inactive);

  @ViewChild('upload_img', { static: false }) upload_img: ElementRef;
  upload_img_type: 'profile_img' | 'banner_img' | 'product_img_url_list';
  public get dataType(): typeof data_type {
    return data_type;
  }
  public get GeneralStatus(): typeof General_Status {
    return General_Status;
  }
  public get AppointmentStatus(): typeof Appointment_Status {
    return Appointment_Status;
  }

  question_options_list = ['Yes', "No"]
  product_categories_list = [];

  @ViewChild('autocompleteInput') autocompleteInput: ElementRef<HTMLInputElement>;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  constructor(
    public util: UtilService,
    private httpService: HttpService,
    private storageService: StorageService,
    private modalService: ModalService,
    private dataService: DataService
  ) { }

  ngOnInit() {
    // console.log(this.data);
    if (this.type == 'create' && this.data_type) {
      this.data = this.storageService.createInitData(this.data_type);
    }

    if (this.type != 'read' && (this.data_type == this.dataType.appointment_data || this.data_type == this.dataType.reservation_timeslot_data)) {
      this.ava_reservation_timeslot_data_list = this.dataService.getAvailableReservationTimeslot();
    }
    if (this.type != 'read' && this.data_type == this.dataType.product_data) {
      this.product_categories_list = this.dataService.getAllProductCategory();
    }

    if (this.data.data_type == data_type.order_data){
      this.data['customer_data'] = this.storageService.LOCAL_STORE.customer_data_list.value.find(d => d.id == this.data.customer_id);
    }
  }


  triggerImgUpload(upload_img_type) {
    if (this.upload_img == null) {
      return;
    }
    this.upload_img_type = upload_img_type;
    this.upload_img.nativeElement.click();
  }
  uploadImg() {
    if (this.upload_img == null || this.util.isLoading) {
      return;
    }
    const fileList: FileList = this.upload_img.nativeElement.files;
    if (fileList && fileList.length > 0) {
      this.util.firstFileToBase64(fileList[0]).then(async (base64: string) => {
        console.log(base64);


        switch (this.upload_img_type) {
          case 'profile_img':
            this.data.profile_img = base64;
            break;
          case 'banner_img':
            this.data.img_url = base64;
            break;
          case 'product_img_url_list':
            this.data.product_img_url.push(base64);
            break;

          default:
            break;
        }

        // let send_data = {
        //   file_name: '',
        //   file_type: this.util.getFileType(fileList[0].type),
        //   base64: base64
        // }
        // this.util.isLoading = true;
        // const upload_base64_to_server = await this.httpService.UploadBase64FileToServer(send_data);
        // this.util.isLoading = false;
        // if (upload_base64_to_server.result == "success") {
        //   this.payment_data.payment_order_data = upload_base64_to_server.data;
        // }
        // else {
        //   this.commonService.openErrorSnackBar("無法上載檔案");
        // }
      })
    }
  }

  async submitForm() {
    // if (this.data.data_type == data_type.question_data) {
    //   console.log(this.data);
    //   return;
    // }
    console.log(this.data);
    const formChecking = await this.validForm();
    console.log("formChecking ", formChecking);
    console.log(this.data_type);
    if (formChecking) {
      this.storageService.saveToDatabase(this.type, this.data_type, this.data);
      
      this.util.openSnackBar("Data successfully created");
      this.modalService.closeModal();
      setTimeout(() => {
        if (this.data_type == this.dataType.appointment_data || this.data_type == this.dataType.reservation_timeslot_data) {
          this.dataService.getAvailableReservationTimeslot();
        }
      }, 1000);
    }
  }



  validForm(): Promise<any> {
    return new Promise((resolve, reject) => {
      let error_msg = null;
      switch (this.data.data_type) {
        // case data_type.user_data:
        //   if (this.data.type == null) {
        //     error_msg = "Please select user type";
        //   }
        //   break;
        case data_type.reservation_timeslot_data:
          let reservation_timeslot_data: Reservation_Timeslot = this.data;
          if (reservation_timeslot_data.date == null) {
            error_msg = "Please select date";
          }
          if (reservation_timeslot_data.start_time == null) {
            error_msg = "Please select start time";
          }
          if (reservation_timeslot_data.end_time == null) {
            error_msg = "Please select end time";
          }
          if (Number(reservation_timeslot_data.start_time.replace(':', '')) > Number(reservation_timeslot_data.end_time.replace(':', ''))) {
            error_msg = "End time must be after start time";
          }
          if (reservation_timeslot_data.price == "") {
            error_msg = "Please input price";
          }
          break;
        case data_type.reservation_timeslot_data:
          let appointment_data: Appointment = this.data;
          if (!appointment_data.reservation_timeslot_id) {
            error_msg = "Please select appointment date";
          }
          if (!appointment_data.customer_id) {
            error_msg = "Please select customer";
          }
          if (!appointment_data.partner_id) {
            error_msg = "Please select partner";
          }
          break;
        case data_type.question_data:
          let question_data: Question = this.data;
          if (question_data.title == '') {
            error_msg = "Please input title";
          }
          if (question_data.options.length < 2) {
            error_msg = "At least two options for a question";
          }
          break;
        case data_type.notification_data:
          let notification_data: Notification = this.data;
          if (notification_data.title == '') {
            error_msg = "Please input title";
          }
          if (notification_data.customer_id_list.length <= 0) {
            error_msg = "Please select customer";
          }
          break;

        default:
          break;
      }
      if (error_msg) {
        this.util.openErrorSnackBar(error_msg);
        resolve(false);
      }
      else {
        resolve(true);
      }
    });
  }

  searchIonicSelect(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();

    if (this.data_type == data_type.reservation_timeslot_data) {
      event.component.items = this.ava_reservation_timeslot_data_list.filter(r_data => {
        return r_data.id.toString().toLowerCase().indexOf(text) !== -1
          || new Date(r_data.date).toISOString().toLowerCase().indexOf(text) !== -1;
      });
    }
    else if (this.data_type == data_type.notification_data) {
      event.component.items = this.customer_data_list_for_notification.filter(r_data => {
        return r_data.display_name.toString().toLowerCase().indexOf(text) !== -1
          || r_data.email.toString().toLowerCase().indexOf(text) !== -1
          || r_data.phone.toString().toLowerCase().indexOf(text) !== -1;
      });
    }

    event.component.endSearch();
  }


  add(event: MatChipInputEvent, array: any[]): void {
    const value = (event.value || '').trim();
    // Add item
    if (value && !array.includes(event.value)) {
      array.push(value);
    }
    // Clear the input value
    event.chipInput!.clear();
  }

  remove(item: string, array: any[]): void {
    const index = array.indexOf(item);
    if (index >= 0) {
      array.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent, array: any[]): void {
    if (!array.includes(event.option.viewValue)) {
      array.push(event.option.viewValue);
    }
    this.autocompleteInput.nativeElement.value = '';
  }

  getCustomerDataById(id: string) {
    return this.storageService.LOCAL_STORE.customer_data_list.value.find(d => d.id == id);
  }

}
