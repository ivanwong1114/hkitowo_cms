export enum data_type {
  system_data = 'system_data',
  admin_data = 'admin_data',
  customer_data = 'customer_data',
  partner_data = 'partner_data',
  reservation_timeslot_data = 'reservation_timeslot_data',
  appointment_data = 'appointment_data',
  banner_data = 'banner_data',
  question_data = 'question_data',
  notification_data = 'notification_data',
  product_data = 'product_data',
  order_data = 'order_data',
  design_order_data = 'design_order_data',
  activity_data = 'activity_data'
}

export interface System {
  customer_vip_threshold: number,
  design_product_list: Design_Product[],
  commission_rate_per_desgin_order: number
}

export interface Design_Product {
 product_type: Design_Product_Type,
 attribute: string,
 price: string
}

export interface Admin{
  id: string,
  data_type: data_type,
  create_datetime: Date, 
  status: General_Status,
  username: string,
  password: string
}

export interface Customer {
  id: string,
  data_type: data_type,
  create_datetime: Date, 
  status: General_Status,
  username: string,
  email: string,
  phone: string,
  display_name: string,
  // month_of_birth: string,
  password: string,
  is_vip: boolean,
  profile_img?: string,
  credit: number,
  referral_code: string
  referrer_code: string
}

export interface Partner {
  id: string,
  data_type: data_type,
  create_datetime: Date, 
  status: General_Status,
  username: string,
  email: string,
  phone: string,
  display_name: string,
  is_staff: boolean,
  // month_of_birth: string,
  password: string,
  profile_img?: string,
}

export enum General_Status{
  created = "created",
  active = "active",
  inactive = "inactive",
  deleted = "deleted"
}

export enum Appointment_Status{
  created = "created",
  updated = "updated",
  completed = "completed",
  cancelled = "cancelled"
}

export enum Order_Status{
  pending = "pending",
  created = "created",
  pending_verification = "pending_verification",
  paid = "paid",
  delivered = "delivered",
  completed = "completed",
  cancelled = "cancelled"
}

export interface Reservation_Timeslot {
  id: string,
  data_type: data_type,
  create_datetime: Date, 
  status: General_Status,
  service_name: string,
  date: Date,
  start_time: string,
  end_time: string,
  quota: number,
  price: string,
  vip_price: string
}

export interface Appointment {
  id: string,
  data_type: data_type,
  create_datetime: Date,
  status: Appointment_Status, 
  reservation_timeslot_id: string,
  customer_id: string | null,
  partner_id: string | null,
  remark: string,
  ranking: number;
}

export interface Banner {
  id: string,
  data_type: data_type,
  create_datetime: Date,
  status: General_Status, 
  img_url: string
}

export interface Question {
  id: string,
  data_type: data_type,
  create_datetime: Date,
  status: General_Status, 
  title: string,
  description: string,
  options: string[],
  multiple: boolean
}

export interface Response {
  id: string,
  data_type: data_type,
  create_datetime: Date,
  status: General_Status, 
  question_id: string,
  customer_id: string,
  options: string[]
}

export interface Notification {
  id: string,
  data_type: data_type,
  create_datetime: Date,
  status: General_Status, 
  customer_id_list: string[],
  title: string
  content: string
}

export interface Product {
  id: string,
  data_type: data_type,
  create_datetime: Date,
  status: General_Status, 
  categories: string[],
  name: string,
  description: string,
  attributes: string[],
  price: string,
  vip_price: string
  product_img_url_list: string[],
  inventory: number,
  redeemable: boolean,
  credit: number
}

export interface Cart {
  product_data: Product,
  quantity: number
}

export interface Payment {
  payment_method: 'bank_transfer' | 'credit_card',
  payment_datetime: Date,
  bank_transfer_img: string,
  card_number: string,
  card_holder: string,
  expiry_date: string,
  cvv: string
  payment_data: any
}

export interface Delivery {
  delivery_method: 'store_pickup' | 'sf_delivery',
  delivery_address: string
}

export interface Order {
  id: string,
  data_type: data_type,
  create_datetime: Date,
  status: Order_Status
  customer_id: string,
  cart: Cart[],
  subtotal: number,
  shipping_fee: number,
  discount: number,
  total_amount: number,
  total_credit: number
  payment_data: Payment,
  delivery_data: Delivery,
  is_vip: boolean,
  is_redeemption: boolean
}

export interface Design_Order {
  id: string,
  data_type: data_type,
  create_datetime: Date,
  status: Order_Status
  customer_id: string,
  customer_phone: string,
  customer_name: string,
  customer_email: string,
  partner_id: string,
  design_data: any
  product_type: Design_Product_Type,
  product_detail: string,
  design_product: Design_Product,
  quantity: number,
  subtotal: number,
  shipping_fee: number,
  discount: number,
  total_amount: number,
  commission: number,
  payment_data: Payment,
  delivery_data: Delivery,
  signature_image: string,
  quotation_html: string
}

export enum Design_Product_Type {
  tshirt = 'tshirt',
  phone_case = 'phone_case'
}

export interface Activity {
  id: string,
  data_type: data_type,
  create_datetime: Date,
  title: 'Change appointment status' | 'Create new design order' | 'Login',
  partner_id: string,
}

export enum Activity_Title {
  change_appointment_status = 'Change appointment status',
  create_new_design_order = 'Create new design order',
  login = 'Login'
}