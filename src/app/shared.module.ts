
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { TableComponent } from './components/table/table.component';
import { FormComponent } from './components/form/form.component';
import { HeaderComponent } from './components/header/header.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { IonicSelectableModule } from 'ionic-selectable';
import { OrderComponent } from './components/app/order/order.component';
import { ProductListComponent } from './components/app/product-list/product-list.component';
import { QrCodeScannerComponent } from './components/qr-code-scanner/qr-code-scanner.component';

@NgModule({
  declarations: [
    TableComponent,
    FormComponent,
    HeaderComponent,
    OrderComponent,
    ProductListComponent,
    QrCodeScannerComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    MaterialModule,
    NgxMaterialTimepickerModule,
    IonicSelectableModule,
  ],
  exports: [
    TableComponent,
    FormComponent,
    HeaderComponent,
    OrderComponent,
    ProductListComponent,
    QrCodeScannerComponent
  ]
})
export class SharedModule { }
