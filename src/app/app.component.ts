import { ChangeDetectorRef, Component, HostListener } from '@angular/core';

import { MenuController, Platform } from '@ionic/angular';
// import { SplashScreen } from '@ionic-native/splash-screen/ngx';
// import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { UtilService } from './services/util.service';
import { Location } from '@angular/common';
import { AuthService } from './services/auth.service';
import { StorageService } from './services/storage.service';
import { StateService } from './services/state.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public selectedIndex = 0;
  public appPages = [
    // {
    //   title: 'Blank',
    //   url: 'blank',
    //   icon: 'storefront'
    // },
    // {
    //   title: 'Home',
    //   url: 'home',
    //   icon: 'storefront'
    // },
    {
      title: 'Customer',
      url: 'customer',
      icon: 'people'
    },
    {
      title: 'Partner',
      url: 'partner',
      icon: 'people'
    },
    {
      title: 'Reservation',
      url: 'reservation',
      icon: 'calendar'
    },
    {
      title: 'Appointment',
      url: 'appointment',
      icon: 'calendar'
    },
    {
      title: 'Product',
      url: 'product',
      icon: 'bag-handle'
    },
    {
      title: 'Order',
      url: 'order',
      icon: 'file-tray-full'
    },
    {
      title: 'Redeem',
      url: 'redeem',
      icon: 'file-tray-full'
    },
    {
      title: 'Design Order',
      url: 'design-order',
      icon: 'file-tray-stacked'
    },

    {
      title: 'Banner',
      url: 'banner',
      icon: 'image'
    },
    {
      title: 'Questionnaire',
      url: 'question',
      icon: 'help-circle'
    },
    // {
    //   title: 'Notification',
    //   url: 'notification',
    //   icon: 'notifications'
    // },
    {
      title: 'System',
      url: 'system',
      icon: 'settings'
    },
    // {
    //   title: '得獎者',
    //   url: 'winner',
    //   icon: 'trophy'
    // },

  ];

  current_url = "";

  isCMS = environment.isCMS;

  constructor(
    public platform: Platform,
    private router: Router,
    private location: Location,
    public menuCtrl: MenuController,
    public utilService: UtilService,
    public authService: AuthService,
    private storageService: StorageService,
    private state: StateService
  ) {
    // // Use matchMedia to check the user preference
    // const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    // console.log(prefersDark.matches);
    // this.toggleDarkTheme(prefersDark.matches);
    // // Listen for changes to the prefers-color-scheme media query
    // prefersDark.addListener((mediaQuery) => this.toggleDarkTheme(mediaQuery.matches));
    // // Add or remove the "dark" class based on if the media query matches

    this.initializeApp();
  }


  initializeApp() {
    this.platform.ready().then(() => {
      // let dark_mode = localStorage.getItem("dark_mode");
      // if (dark_mode != undefined || dark_mode != null) {
      //   this.utilService.dark_mode = (dark_mode == 'false' ? false : true);
      //   document.body.classList.toggle('dark-theme', (dark_mode == 'false' ? false : true));
      // }

      this.state.initStateService();

      this.storageService.init();

      
      if (!this.platform.is('cordova')){
        this.router.events.subscribe(val => {
          if (this.location.path() != "") {
            this.current_url = this.router.url;
            this.selectedIndex = this.appPages.findIndex(d => d.url == this.location.path());
          }
        });
      }

    });
  }

  refresh() {
    location.reload();
  }


  // toggleDarkTheme(shouldAdd) {
  //   document.body.classList.toggle('dark-theme', shouldAdd);
  // }

}
