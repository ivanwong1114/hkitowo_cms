import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = environment.isCMS == false ? [
  {
    path: '',
    loadChildren: () => import('./pages/app/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  // {
  //   path: 'tabs',
  //   loadChildren: () => import('./pages/app/tabs/tabs.module').then(m => m.TabsPageModule),
  //   canActivate: [AuthGuard]
  // },
  // {
  //   path: 'home',
  //   loadChildren: () => import('./pages/app/home/home.module').then( m => m.HomePageModule),
  //   canActivate: [AuthGuard]
  // },
  {
    path: 'login',
    loadChildren: () => import('./pages/app/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/app/register/register.module').then( m => m.RegisterPageModule),
  },
  {
    path: 'product-detail',
    loadChildren: () => import('./pages/app/product-detail/product-detail.module').then( m => m.ProductDetailPageModule),
    // canActivate: [AuthGuard]
  },
  {
    path: 'shopping-bag',
    loadChildren: () => import('./pages/app/shopping-bag/shopping-bag.module').then( m => m.ShoppingBagPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'checkout',
    loadChildren: () => import('./pages/app/checkout/checkout.module').then( m => m.CheckoutPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'redeem',
    loadChildren: () => import('./pages/app/redeem/redeem.module').then( m => m.RedeemPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/app/dashboard/dashboard.module').then( m => m.DashboardPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'quotation',
    loadChildren: () => import('./pages/app/quotation/quotation.module').then( m => m.QuotationPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'checkout-completed',
    loadChildren: () => import('./pages/app/checkout-completed/checkout-completed.module').then( m => m.CheckoutCompletedPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'order-detail',
    loadChildren: () => import('./pages/app/order-detail/order-detail.module').then( m => m.OrderDetailPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'signature',
    loadChildren: () => import('./pages/app/signature/signature.module').then( m => m.SignaturePageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'quotation-preview',
    loadChildren: () => import('./pages/app/quotation-preview/quotation-preview.module').then( m => m.QuotationPreviewPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'partner-appointment',
    loadChildren: () => import('./pages/app/partner-appointment/partner-appointment.module').then( m => m.PartnerAppointmentPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'report',
    loadChildren: () => import('./pages/app/report/report.module').then( m => m.ReportPageModule),
    canActivate: [AuthGuard]
  }
] : [
    //cms

    {
      path: '',
      // loadChildren: () => import('./pages/cms/customer/customer.module').then( m => m.CustomerPageModule)
      redirectTo: '/customer',
      pathMatch: 'full'
    },
    {
      path: 'login',
      loadChildren: () => import('./pages/cms/login/login.module').then( m => m.LoginPageModule)
    },
    {
      path: 'customer',
      loadChildren: () => import('./pages/cms/customer/customer.module').then( m => m.CustomerPageModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'partner',
      loadChildren: () => import('./pages/cms/partner/partner.module').then( m => m.PartnerPageModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'reservation',
      loadChildren: () => import('./pages/cms/reservation/reservation.module').then( m => m.ReservationPageModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'appointment',
      loadChildren: () => import('./pages/cms/appointment/appointment.module').then( m => m.AppointmentPageModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'banner',
      loadChildren: () => import('./pages/cms/banner/banner.module').then( m => m.BannerPageModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'question',
      loadChildren: () => import('./pages/cms/question/question.module').then( m => m.QuestionPageModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'notification',
      loadChildren: () => import('./pages/cms/notification/notification.module').then( m => m.NotificationPageModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'product',
      loadChildren: () => import('./pages/cms/product/product.module').then( m => m.ProductPageModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'order',
      loadChildren: () => import('./pages/cms/order/order.module').then( m => m.OrderPageModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'design-order',
      loadChildren: () => import('./pages/cms/design-order/design-order.module').then( m => m.DesignOrderPageModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'redeem',
      loadChildren: () => import('./pages/cms/redeem/redeem.module').then( m => m.RedeemPageModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'system',
      loadChildren: () => import('./pages/cms/system/system.module').then( m => m.SystemPageModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'sales-summary',
      loadChildren: () => import('./pages/cms/sales-summary/sales-summary.module').then( m => m.SalesSummaryPageModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'activity-log',
      loadChildren: () => import('./pages/cms/activity-log/activity-log.module').then( m => m.ActivityLogPageModule),
      canActivate: [AuthGuard]
    },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
