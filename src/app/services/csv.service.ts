import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CsvService {

    constructor() { }

    downloadFile(data, type, filename = 'data') {
        let design_order_sales_header = ['Create datetime', 'status', 'Customer phone', 'Customer name', 'Customer email', "Product type", "Product detail", "Product attribute", "Quantity", "Total amount", "Commission"];
        let order_sales_header = ['Create datetime', 'Status', 'Customer phone', 'Customer email', "Quantity", "Total amount"];
        let partner_performance = ['Partner phone', 'Partner email', 'Total quantity', 'Total sales', 'Total commission'];
        let commission = ['Partner phone', 'Partner email', 'Total commission'];
        let header = null;
        switch (type) {
            case 'design-order-sales-summary':
                header = design_order_sales_header;
                break;
            case 'order-sales-summary':
                header = order_sales_header;
            case 'monthly-partner-performance':
                header = partner_performance;
                break;
            case 'monthly-commission':
                header = commission;
                break;
            default:
                break;
        }

        let csvData = this.ConvertToCSV(data, header);
        console.log(csvData)
        let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
        let dwldLink = document.createElement("a");
        let url = URL.createObjectURL(blob);
        let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
        if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
            dwldLink.setAttribute("target", "_blank");
        }
        dwldLink.setAttribute("href", url);
        dwldLink.setAttribute("download", filename + ".csv");
        dwldLink.style.visibility = "hidden";
        document.body.appendChild(dwldLink);
        dwldLink.click();
        document.body.removeChild(dwldLink);
    }

    ConvertToCSV(objArray, headerList) {
        let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        let str = '';
        let row = 'No,';

        for (let index in headerList) {
            row += headerList[index] + ',';
        }
        row = row.slice(0, -1);
        str += row + '\r\n';
        for (let i = 0; i < array.length; i++) {
            let line = (i + 1) + '';
            for (let index in headerList) {
                let head = headerList[index];

                line += ',' + array[i][head];
            }
            str += line + '\r\n';
        }
        return str;
    }
}
