import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormComponent } from '../components/form/form.component';
import { TableComponent } from '../components/table/table.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(
    private modalController: ModalController
  ) { }

  async presentTableModal(type: 'edit' | 'read' | 'create', data: any, data_type?) {
    const modal = await this.modalController.create({
      component: FormComponent,
      cssClass: 'view-modal',
      componentProps: {
        type: type,
        data: data,
        data_type: data ? data.data_type : data_type
      },
      animated: false
    });
    return await modal.present();
  }

  closeModal(){
    this.modalController.dismiss();
  }

}
