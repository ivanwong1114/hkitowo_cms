import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

const headers = { headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };

export interface ApiResponse {
  result: 'success' | 'fail' | 'error';
  data: any;
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient
  ) {
  }

  // GetSystemData(): Promise<any> {
  //   let body = new URLSearchParams();
  //   body.set('get_system_data', "");

  //   return new Promise((resolve, reject) => {
  //     this.http.post((environment.env == 'dev' ? environment.dev_api_url : environment.prod_api_url), body.toString(), headers).subscribe(async data => {
  //       let temp = data;
  //       if (temp != null) {
  //         if (temp['result'] == "success") {
  //           resolve(temp);
  //         } else if (temp['result'] == "fail") {
  //           resolve(temp);
  //         } else if (temp['result'] == "error") {
  //           reject(temp);
  //         }
  //       }
  //       else {
  //         reject(null);
  //       }
  //     }),
  //       async error => {
  //         return error;
  //       };
  //   });
  // }

  // NewUser(send_date): Promise<any> {
  //   let body = new URLSearchParams();
  //   body.set('new_user', JSON.stringify(send_date));

  //   return new Promise((resolve, reject) => {
  //     this.http.post((environment.env == 'dev' ? environment.dev_api_url : environment.prod_api_url), body.toString(), headers).subscribe(async data => {
  //       let temp = data;
  //       if (temp != null) {
  //         if (temp['result'] == "success") {
  //           resolve(temp);
  //         } else if (temp['result'] == "fail") {
  //           resolve(temp);
  //         } else if (temp['result'] == "error") {
  //           reject(temp);
  //         }
  //       }
  //       else {
  //         reject(null);
  //       }
  //     }),
  //       async error => {
  //         return error;
  //       };
  //   });
  // }

  AdminLogin(send_date): Promise<any> {
    let body = new URLSearchParams();
    body.set('admin_login', JSON.stringify(send_date));

    return new Promise((resolve, reject) => {
      let res: ApiResponse = {
        result: 'success',
        data: null,
        message: ''
      } 
      resolve(res);
    });
  }

  // GetUserDataAndTotalNumberBySortingAndLimitOrSearch(send_date): Promise<any> {
  //   let body = new URLSearchParams();
  //   body.set('get_user_data_and_total_number_by_sorting_and_limit_or_search', JSON.stringify(send_date));

  //   return new Promise((resolve, reject) => {
  //     this.http.post((environment.env == 'dev' ? environment.dev_api_url : environment.prod_api_url), body.toString(), headers).subscribe(async data => {
  //       let temp = data;
  //       if (temp != null) {
  //         if (temp['result'] == "success") {
  //           resolve(temp);
  //         } else if (temp['result'] == "fail") {
  //           resolve(temp);
  //         } else if (temp['result'] == "error") {
  //           reject(temp);
  //         }
  //       }
  //       else {
  //         reject(null);
  //       }
  //     }),
  //       async error => {
  //         return error;
  //       };
  //   });
  // }

  // AddImgUrlToWorkImgUrlList(send_date): Promise<any> {
  //   let body = new URLSearchParams();
  //   body.set('add_img_url_to_work_img_url_list', JSON.stringify(send_date));

  //   return new Promise((resolve, reject) => {
  //     this.http.post((environment.env == 'dev' ? environment.dev_api_url : environment.prod_api_url), body.toString(), headers).subscribe(async data => {
  //       let temp = data;
  //       if (temp != null) {
  //         if (temp['result'] == "success") {
  //           resolve(temp);
  //         } else if (temp['result'] == "fail") {
  //           resolve(temp);
  //         } else if (temp['result'] == "error") {
  //           reject(temp);
  //         }
  //       }
  //       else {
  //         reject(null);
  //       }
  //     }),
  //       async error => {
  //         return error;
  //       };
  //   });
  // }

  // RemoveImgUrlToWorkImgUrlList(send_date): Promise<any> {
  //   let body = new URLSearchParams();
  //   body.set('remove_img_url_to_work_img_url_list', JSON.stringify(send_date));

  //   return new Promise((resolve, reject) => {
  //     this.http.post((environment.env == 'dev' ? environment.dev_api_url : environment.prod_api_url), body.toString(), headers).subscribe(async data => {
  //       let temp = data;
  //       if (temp != null) {
  //         if (temp['result'] == "success") {
  //           resolve(temp);
  //         } else if (temp['result'] == "fail") {
  //           resolve(temp);
  //         } else if (temp['result'] == "error") {
  //           reject(temp);
  //         }
  //       }
  //       else {
  //         reject(null);
  //       }
  //     }),
  //       async error => {
  //         return error;
  //       };
  //   });
  // }

  // UpdateSystemData(send_date): Promise<any> {
  //   let body = new URLSearchParams();
  //   body.set('update_system_data', JSON.stringify(send_date));

  //   return new Promise((resolve, reject) => {
  //     this.http.post((environment.env == 'dev' ? environment.dev_api_url : environment.prod_api_url), body.toString(), headers).subscribe(async data => {
  //       let temp = data;
  //       if (temp != null) {
  //         if (temp['result'] == "success") {
  //           resolve(temp);
  //         } else if (temp['result'] == "fail") {
  //           resolve(temp);
  //         } else if (temp['result'] == "error") {
  //           reject(temp);
  //         }
  //       }
  //       else {
  //         reject(null);
  //       }
  //     }),
  //       async error => {
  //         return error;
  //       };
  //   });
  // }

  // DeleteDataByDataTypeAndId(send_date): Promise<any> {
  //   let body = new URLSearchParams();
  //   body.set('delete_data_by_data_type_and_id', JSON.stringify(send_date));

  //   return new Promise((resolve, reject) => {
  //     this.http.post((environment.env == 'dev' ? environment.dev_api_url : environment.prod_api_url), body.toString(), headers).subscribe(async data => {
  //       let temp = data;
  //       if (temp != null) {
  //         if (temp['result'] == "success") {
  //           resolve(temp);
  //         } else if (temp['result'] == "fail") {
  //           resolve(temp);
  //         } else if (temp['result'] == "error") {
  //           reject(temp);
  //         }
  //       }
  //       else {
  //         reject(null);
  //       }
  //     }),
  //       async error => {
  //         return error;
  //       };
  //   });
  // }

  // AddWinnerToWinnerList(send_date): Promise<any> {
  //   let body = new URLSearchParams();
  //   body.set('add_winner_to_winner_list', JSON.stringify(send_date));

  //   return new Promise((resolve, reject) => {
  //     this.http.post((environment.env == 'dev' ? environment.dev_api_url : environment.prod_api_url), body.toString(), headers).subscribe(async data => {
  //       let temp = data;
  //       if (temp != null) {
  //         if (temp['result'] == "success") {
  //           resolve(temp);
  //         } else if (temp['result'] == "fail") {
  //           resolve(temp);
  //         } else if (temp['result'] == "error") {
  //           reject(temp);
  //         }
  //       }
  //       else {
  //         reject(null);
  //       }
  //     }),
  //       async error => {
  //         return error;
  //       };
  //   });
  // }


  // RemoveWinnerToWinnerList(send_date): Promise<any> {
  //   let body = new URLSearchParams();
  //   body.set('remove_winner_to_winner_list', JSON.stringify(send_date));

  //   return new Promise((resolve, reject) => {
  //     this.http.post((environment.env == 'dev' ? environment.dev_api_url : environment.prod_api_url), body.toString(), headers).subscribe(async data => {
  //       let temp = data;
  //       if (temp != null) {
  //         if (temp['result'] == "success") {
  //           resolve(temp);
  //         } else if (temp['result'] == "fail") {
  //           resolve(temp);
  //         } else if (temp['result'] == "error") {
  //           reject(temp);
  //         }
  //       }
  //       else {
  //         reject(null);
  //       }
  //     }),
  //       async error => {
  //         return error;
  //       };
  //   });
  // }

  // GetAllWinnerListWithUserData(send_date): Promise<any> {
  //   let body = new URLSearchParams();
  //   body.set('get_all_winner_list_with_user_data', JSON.stringify(send_date));

  //   return new Promise((resolve, reject) => {
  //     this.http.post((environment.env == 'dev' ? environment.dev_api_url : environment.prod_api_url), body.toString(), headers).subscribe(async data => {
  //       let temp = data;
  //       if (temp != null) {
  //         if (temp['result'] == "success") {
  //           resolve(temp);
  //         } else if (temp['result'] == "fail") {
  //           resolve(temp);
  //         } else if (temp['result'] == "error") {
  //           reject(temp);
  //         }
  //       }
  //       else {
  //         reject(null);
  //       }
  //     }),
  //       async error => {
  //         return error;
  //       };
  //   });
  // }



}
