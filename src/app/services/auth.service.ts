import { Injectable } from '@angular/core';
import { Platform, NavController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject, from, of, Subscription } from 'rxjs';
import { switchMap, map, take } from 'rxjs/operators';
import { UtilService } from './util.service';
import { HttpService } from './http.service';
import { Customer, data_type, Partner } from '../interface';
import { StorageService } from './storage.service';
import { environment } from 'src/environments/environment';
import { P } from '@angular/cdk/keycodes';

const ADMIN_KEY = 'HKITOWO_ADMIN_DATA';
const USER_KEY = 'HKITOWO_USER_DATA';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  adminData = new BehaviorSubject(null);
  userData = new BehaviorSubject(null);

  customerDataListSub$$: Subscription;
  partnerDataListSub$$: Subscription;

  constructor(
    private platform: Platform,
    private router: Router,
    private nav: NavController,
    private alertController: AlertController,
    public utilService: UtilService,
    private httpService: HttpService,
    private storageService: StorageService
  ) {
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });

  }

  ifLoggedIn() {
    let data = JSON.parse(localStorage.getItem(environment.isCMS ? ADMIN_KEY : USER_KEY));
    if (data) {
      environment.isCMS ? this.adminData.next((data)) : this.userData.next(data);
    }
    console.log(`${environment.isCMS ? 'admin: ' : 'user: '}`, data);
  }

  Logout() {
    environment.isCMS ? this.adminData.next(null) : this.userData.next(null);
    window.localStorage.removeItem(environment.isCMS ? ADMIN_KEY : USER_KEY);
    if (!this.router.url.includes('login')) {
      window.location.href = './login';
    }

    if (this.customerDataListSub$$){
      this.customerDataListSub$$.unsubscribe();
    }
    if (this.partnerDataListSub$$){
      this.partnerDataListSub$$.unsubscribe();
    }

 
  }


  updateCustomerOnChange(){
    this.customerDataListSub$$ = this.storageService.LOCAL_STORE.customer_data_list.subscribe((customer_data_list: Customer[]) => {
      if (customer_data_list && customer_data_list.length > 0){
        let customer_data = customer_data_list.find(c => c.id == this.userData.value.id);
        if (!customer_data){
          this.Logout();
        }
        else if (JSON.stringify(customer_data) != JSON.stringify(this.userData.value)){
          this.userData.next(customer_data);
        }
      }
    })
  }

  updatePartnerOnChange(){
    this.partnerDataListSub$$ = this.storageService.LOCAL_STORE.partner_data_list.subscribe((partner_data_list: Partner[]) => {
      if (partner_data_list && partner_data_list.length > 0){
        let partner_data = partner_data_list.find(c => c.id == this.userData.value.id);
        if (!partner_data){
          this.Logout();
        }
        else if (JSON.stringify(partner_data) != JSON.stringify(this.userData.value)){
          this.userData.next(partner_data);
        }
      }
    })
  }


  async AdminLogin(username: string, password: string) {
    console.log(this.storageService.LOCAL_STORE.admin_data_list.value);
    let index = this.storageService.LOCAL_STORE.admin_data_list.value.findIndex(d => d.username == username && d.password == password);
    if (index != -1){
      let admin_data = this.storageService.LOCAL_STORE.admin_data_list.value[index];
      this.adminData.next(admin_data);
      localStorage.setItem(ADMIN_KEY, JSON.stringify(admin_data));
      return true;
    }
    else{
      this.utilService.presentToast("用戶名稱或密碼錯誤");
      return null;
    }
  }

  async AppLogin(type: data_type.customer_data | data_type.partner_data, email: string, password: string) {
    let index = type == data_type.customer_data ? this.storageService.LOCAL_STORE.customer_data_list.value.findIndex(d => d.email == email && d.password == password) : this.storageService.LOCAL_STORE.partner_data_list.value.findIndex(d => d.email == email && d.password == password);
    if (index != -1) {
      let user_data = type == data_type.customer_data ? this.storageService.LOCAL_STORE.customer_data_list.value[index] : this.storageService.LOCAL_STORE.partner_data_list.value[index];
      this.userData.next(user_data);
      localStorage.setItem(USER_KEY, JSON.stringify(user_data));
      type == data_type.customer_data ? this.updateCustomerOnChange() : this.updatePartnerOnChange();
      return true;
    }
    else {
      this.utilService.presentToast("電郵或密碼錯誤");
      return null;
    }
  }

  updateUserData(user_data){
    localStorage.setItem(USER_KEY, JSON.stringify(user_data));
  }

  deleteAccount(){
    let user_data = this.userData.value;
    let index = user_data.data_type == data_type.customer_data ? this.storageService.LOCAL_STORE.customer_data_list.value.findIndex(d => d.id == user_data.id) : this.storageService.LOCAL_STORE.partner_data_list.value.findIndex(d => d.id == user_data.id);
    if (index != -1) {
      if (user_data.data_type == data_type.customer_data){
        this.storageService.LOCAL_STORE.customer_data_list.next(this.storageService.LOCAL_STORE.customer_data_list.value.filter((c: Customer) => c.id != user_data.id));
      }
      else if (user_data.data_type == data_type.partner_data){
        this.storageService.LOCAL_STORE.partner_data_list.next(this.storageService.LOCAL_STORE.partner_data_list.value.filter((p: Partner) => p.id != user_data.id));
      }
 
      this.Logout();
    }
  }

  // directLogout() {
  //   this.adminToken.next(null);
  //   window.localStorage.removeItem("admin_data");
  //   this.utilService.OpenPage('/home');
  // }

  // SyncUserDate(event?) {
  //   if (this.getStoreAccountData() == null) {
  //     if (event != undefined) {
  //       event.target.complete();
  //     }
  //     return;
  //   }
  //   let send_data = {
  //     data_type: this.getStoreAccountData().data_type, 
  //     application_key: this.getStoreAccountData().application_key
  //   }
  //   this.httpService.GetSingleDataByDataTypeAndApplicationKey(send_data).then(res => {
  //     console.log(res);
  //     if (event != undefined) {
  //       event.target.complete();
  //     }
  //     if (res.result == "success" && res.data != null) {
  //       if (res.data.disabled) {
  //         this.utilService.openSnackBar("帳號已被停用", null, 5000);
  //         this.Logout();
  //         return;
  //       }
  //       if (res.data.password_data != this.getStoreAccountData().password_data) {
  //         this.utilService.openSnackBar("密碼已被更改，請重新登入", null,  5000);
  //         this.Logout();
  //         return;
  //       }
  //       this.updateUserData(res.data);
  //     }
  //     else {
  //       this.Logout();
  //     }
  //   });
  // }

  isAuthenticated() {
    return environment.isCMS ? this.adminData.value : this.userData.value;
  }


  resetPartnerPassword(partner_data: Partner){
    let new_password = this.utilService.genereateRandomPassword();
    partner_data.password = new_password;
    this.storageService.saveToDatabase('edit', partner_data.data_type, partner_data);
    this.utilService.openSnackBar(`New password is ${partner_data.password}`, 'OK', 10000000000);
  }

}
