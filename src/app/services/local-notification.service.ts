import { Injectable } from '@angular/core';
import { LocalNotifications } from '@awesome-cordova-plugins/local-notifications/ngx';
import { Appointment } from '../interface';

@Injectable({
  providedIn: 'root'
})
export class LocalNotificationService {

  constructor(
    private localNotifications: LocalNotifications
  ) { }


  createAppointmentNotification(appointment_data){
    this.localNotifications.schedule({
      id: appointment_data.id,
      text: `明天同樣時間將有預約服務`,
      trigger: {at: new Date(new Date(appointment_data.reservation_timeslot_data.date).getTime() - 86400000)},
      // data: { secret: key }
    });
    // {{item?.reservation_timeslot_data?.date | date:'yyyy-MM-dd'}}
                // {{item?.reservation_timeslot_data?.start_time}}-{{item?.reservation_timeslot_data?.end_time}}
  }

  cancelAppointmentNotification(appointment_data: Appointment){
    this.localNotifications.cancel(appointment_data.id);
  }

}
