import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { Partner, Product } from '../interface';

export enum DEVICE_PLATFORM {
  ios = 'ios',
  android = 'android',
  pwa = "pwa",
}

@Injectable({
  providedIn: 'root'
})
export class StateService {

  device_platform$ = new BehaviorSubject<DEVICE_PLATFORM>(null);

  current_product$ = new BehaviorSubject<Product>(null);

  qr_code_result$ = new BehaviorSubject<string>(null);

  design_data$ = new BehaviorSubject<any>(null);

  sales_summary_partner = new BehaviorSubject<Partner>(null);
  
  activity_log_partner = new BehaviorSubject<Partner>(null);

  constructor(
    private platform: Platform
  ) { }

  initStateService(){
    this.getPlatform();
  }

  private getPlatform() {
    let platform: DEVICE_PLATFORM;
    if (this.platform.is('ios') && this.platform.is('cordova')) {
      platform = DEVICE_PLATFORM.ios;
    }
    // else if (
    //   (this.platform.is('android')
    //     || this.device.platform?.toLowerCase() === 'android') && this.platform.is('cordova')) {
    else if (this.platform.is('android') && this.platform.is('cordova')) {
      platform = DEVICE_PLATFORM.android;
    }
    else {
      platform = DEVICE_PLATFORM.pwa;
    }
    this.device_platform$.next(platform);
    return platform
  };
}
