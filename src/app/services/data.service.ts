import { A } from '@angular/cdk/keycodes';
import { Injectable } from '@angular/core';
import { Appointment, Appointment_Status, data_type, Order, Product, Reservation_Timeslot } from '../interface';
import { AuthService } from './auth.service';
import { StorageService } from './storage.service';
import { UtilService } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private storageService: StorageService,
    private authService: AuthService,
    private util: UtilService
  ) { }

  
  getAvailableReservationTimeslot() {
    return this.storageService.LOCAL_STORE.reservation_timeslot_data_list.value
      .map((r: Reservation_Timeslot) => {
        {
          r['available_quota'] = r.quota - this.storageService.LOCAL_STORE.appointment_data_list.value.filter((a: Appointment) => a.reservation_timeslot_id == r.id && a.status != Appointment_Status.cancelled && a.status != Appointment_Status.completed).length;
        }
        return r;
      })
      .filter((r: Reservation_Timeslot) => r['available_quota'] > 0 );
    // console.log(this.ava_reservation_timeslot_data_list);
  }

  
  getAllProductCategory() {
    let product_categories_list = [];

    this.storageService.LOCAL_STORE.product_data_list.value.forEach((p: Product) => {
      p.categories.forEach(c => {
        if (!product_categories_list.includes(c)){
          product_categories_list.push(c);
        }
      });
    });
    return product_categories_list;
  }

  getCustomerAppointmentWithReservationData(){
    let user_appointment_list = this.storageService.LOCAL_STORE.appointment_data_list.value.filter(d => d.customer_id == this.authService.userData.value.id);
    user_appointment_list.map((a: Appointment) => {
      a['reservation_timeslot_data'] = this.storageService.LOCAL_STORE.reservation_timeslot_data_list.value.find(d => d.id == a.reservation_timeslot_id);
    });
    return this.util.sortDesc(user_appointment_list, 'create_datetime');
  }

  getPartnerAppointmentWithReservationData(){
    let user_appointment_list = this.storageService.LOCAL_STORE.appointment_data_list.value.filter(d => d.partner_id == this.authService.userData.value.id);
    user_appointment_list.map((a: Appointment) => {
      a['reservation_timeslot_data'] = this.storageService.LOCAL_STORE.reservation_timeslot_data_list.value.find(d => d.id == a.reservation_timeslot_id);
    });
    return this.util.sortDesc(user_appointment_list, 'create_datetime');
  }

  getCustomerOrder(){
    console.log(this.storageService.LOCAL_STORE.order_data_list.value);
    return this.storageService.LOCAL_STORE.order_data_list.value && this.storageService.LOCAL_STORE.order_data_list.value.length > 0 ? this.storageService.LOCAL_STORE.order_data_list.value.filter(d => d.customer_id == this.authService.userData.value.id) : this.storageService.LOCAL_STORE.order_data_list.value;
  }

  getPartnerDesignOrder(partner_id?){
    return this.storageService.LOCAL_STORE.design_order_data_list.value && this.storageService.LOCAL_STORE.design_order_data_list.value.length > 0 ? this.storageService.LOCAL_STORE.design_order_data_list.value.filter(d => d.partner_id == (partner_id ? partner_id :this.authService.userData.value.id)) : this.storageService.LOCAL_STORE.design_order_data_list.value;
  }

  getPartnerActivtyLog(partner_id?){
    return this.storageService.LOCAL_STORE.activity_data_list.value && this.storageService.LOCAL_STORE.activity_data_list.value.length > 0 ? this.storageService.LOCAL_STORE.activity_data_list.value.filter(d => d.partner_id == (partner_id ? partner_id :this.authService.userData.value.id)) : this.storageService.LOCAL_STORE.activity_data_list.value;
  }
  
}
