import { Injectable } from '@angular/core';
import { Cart, Customer, data_type, Design_Order, General_Status, Order, Order_Status, Product } from '../interface';
import { AuthService } from './auth.service';
import { StateService } from './state.service';
import { StorageService } from './storage.service';
import { UtilService } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(
    private storageService: StorageService,
    private authService: AuthService,
    private util: UtilService,
    private state: StateService
  ) { }


  checkCustomerOrderData(){
    if (this.authService.userData.value != null && this.storageService.LOCAL_STORE.customer_order_data.value == null){
      let new_order_data: Order = this.storageService.createInitData(data_type.order_data);
      new_order_data.customer_id = this.authService.userData.value.id;
      new_order_data.is_vip = this.authService.userData.value.is_vip;
      this.storageService.LOCAL_STORE.customer_order_data.next(new_order_data);
    }
  }

  checkPartnerOrderData(){
    if (this.storageService.LOCAL_STORE.partner_design_order_data.value == null){
      let new_design_order_data: Design_Order = this.storageService.createInitData(data_type.design_order_data);
      new_design_order_data.partner_id = this.authService.userData.value.id;
      this.storageService.LOCAL_STORE.partner_design_order_data.next(new_design_order_data);
    }
  }

  addOrRemoveProductFromOrder(product: Product, count: number, is_redeem: boolean){
    // console.log(product);
    // console.log(count);
    if (this.authService.userData.value == null){
      return this.util.presentToast('請先登入');
    }
    let cusomter_order_data:Order = this.storageService.LOCAL_STORE.customer_order_data.value;
    if (cusomter_order_data.cart.length > 0){
        if (cusomter_order_data.is_redeemption && !is_redeem){
          return this.util.presentToast('請先結帳或清空稅換商品');
        }
        else if (cusomter_order_data.is_redeemption == false && is_redeem){
          return this.util.presentToast('請先結帳或清空非稅換商品');
        }
    }
    cusomter_order_data.is_redeemption = is_redeem;
    if (count > 0){
      let index = cusomter_order_data.cart.findIndex(d => d.product_data.id == product.id);
      if (index != -1){
        cusomter_order_data.cart[index].quantity = cusomter_order_data.cart[index].quantity+count;
      }
      else{
        cusomter_order_data.cart.push({
          product_data: product,
          quantity: count
        });
      }

    }else{
      let index = cusomter_order_data.cart.findIndex(d => d.product_data.id == product.id);
      if (index != -1){
        cusomter_order_data.cart[index].quantity = cusomter_order_data.cart[index].quantity+count;
      }
    }
    cusomter_order_data.cart = cusomter_order_data.cart.filter((c: Cart) => c.quantity > 0);
    // this.storageService.saveToDatabase('edit', 'customer_order_data', cusomter_order_data);
    this.storageService.LOCAL_STORE.customer_order_data.next(cusomter_order_data);
    // console.log("customer order: ", cusomter_order_data);
    // this.util.presentToast("成功更新購物車");

    this.calculateOrder();
  }

  calculateOrder(){


    let order_data = this.storageService.LOCAL_STORE.customer_order_data.value;

    if (order_data.is_redeemption == true){
      let total_credit = 0;
      order_data.cart.forEach((c: Cart) => {
        let product_credit = c.product_data.credit;
        total_credit+=(+product_credit*c.quantity);
      });
      order_data.total_credit = total_credit;
    }
    else{
      let subtotal = 0;
      order_data.cart.forEach((c: Cart) => {
        let product_price = this.authService.userData.value.is_vip ? c.product_data.vip_price : c.product_data.price;
        subtotal+=(+product_price*c.quantity);
      });
      let shipping_fee = 0;
      let discount = 0;
      let total_amount = +this.util.roundedToFixed((subtotal+shipping_fee-discount), 1);
  
      order_data.subtotal = subtotal;
      order_data.shipping_fee = shipping_fee;
      order_data.discount = discount;
      order_data.total_amount = total_amount;

    }

    this.storageService.LOCAL_STORE.customer_order_data.next(order_data);
  }

  createNewOrder(){
    const customer_order_data = this.storageService.LOCAL_STORE.customer_order_data.value;
    if (customer_order_data){
    
      this.storageService.saveToDatabase('create', customer_order_data.data_type, customer_order_data);
      this.adjustProductInventory(customer_order_data);
      setTimeout(() => {
        this.storageService.LOCAL_STORE.customer_order_data.next(null);
      }, 500);
    }
  }

  adjustProductInventory(order: Order){
    if (this.storageService.LOCAL_STORE.product_data_list.value){
      order.cart.forEach((cart: Cart) => {
        let index = this.storageService.LOCAL_STORE.product_data_list.value.findIndex(d => d.id == cart.product_data.id);
        if (index != -1){
          const product_data: Product = this.storageService.LOCAL_STORE.product_data_list.value[index];
          product_data.inventory -= cart.quantity;
          this.storageService.saveToDatabase('edit', product_data.data_type, product_data);
        }
    });
    }

  }

  checkCustomerThresholdAndUpgradeVip(){
    let vip_threshold = this.storageService.LOCAL_STORE.system_data.value.customer_vip_threshold;
    console.log(`Vip threshold: ${vip_threshold}`);
    if (vip_threshold != null){
      if (this.storageService.LOCAL_STORE.order_data_list.value && this.storageService.LOCAL_STORE.order_data_list.value){
        let customer_list = this.storageService.LOCAL_STORE.customer_data_list.value.filter((c: Customer) => 
        c.status != General_Status.deleted && c.status != General_Status.inactive && !c.is_vip &&
        this.storageService.LOCAL_STORE.order_data_list.value.findIndex((o: Order) => o.status != Order_Status.cancelled && o.status != Order_Status.pending
        && o.status != Order_Status.pending_verification && o.customer_id == c.id) != -1);
    
        customer_list.forEach((customer_data: Customer) => {
          let customer_accumulated_amount = this.storageService.LOCAL_STORE.order_data_list.value.filter((o: Order) => o.status != Order_Status.cancelled && o.status != Order_Status.pending
          && o.status != Order_Status.pending_verification && o.customer_id == customer_data.id).map((o: Order) => o.total_amount).reduce( (previousValue, currentValue) => previousValue + currentValue, 0);
          console.log(`customer ${customer_data.id} accumulated amount: ${customer_accumulated_amount}`);
          if (vip_threshold <= customer_accumulated_amount){
            customer_data.is_vip = true;
            this.storageService.saveToDatabase('edit', customer_data.data_type, customer_data);
            console.log("customer upgraded to VIP!");
          }
        });
      }
    }

  }

}
