import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { BehaviorSubject } from 'rxjs';
import { skip, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Activity, Activity_Title, Admin, Appointment, Appointment_Status, Banner, Customer, data_type, Design_Order, Design_Product_Type, General_Status, Notification, Order, Order_Status, Partner, Product, Question, Reservation_Timeslot, Response, System } from '../interface';
// import { AuthService } from './auth.service';
import { LocalNotificationService } from './local-notification.service';
import { OrderService } from './order.service';
import { UtilService } from './util.service';

export enum Data_Type_Mapping_Local_Storage {
  admin_data = "admin_data_list",
  customer_data = "customer_data_list",
  partner_data = "partner_data_list",
  reservation_timeslot_data = "reservation_timeslot_data_list",
  appointment_data = "appointment_data_list",
  banner_data = "banner_data_list",
  question_data = "question_data_list",
  response_data = "response_data_list",
  notification_data = "notification_data_list",
  product_data = "product_data_list",
  order_data = "order_data_list",
  design_order_data = "design_order_data_list",
  activity_data = "activity_data_list"
}

export enum Local_Storage {
  data_last_updated = "data_last_updated",
  first_enter = "first_enter",
  admin_data_list = "admin_data_list",
  customer_data_list = "customer_data_list",
  reservation_timeslot_data_list = "reservation_timeslot_data_list",
  appointment_data_list = "appointment_data_list",
  banner_data_list = "banner_data_list",
  question_data_list = "question_data_list",
  response_data_list = "response_data_list",
  notification_data_list = "notification_data_list",
  product_data_list = "product_data_list",
  customer_order_data = "customer_order_data",
  order_data_list = "order_data_list",
  partner_design_order_data = "partner_design_order_data",
  design_order_data_list = "design_order_data_list",
  activity_data_list = "activity_data_list",
}

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  public get dataType(): typeof data_type {
    return data_type;
  }

  private _storage: Storage | null = null;

  LOCAL_STORE = {
    data_last_updated: new BehaviorSubject<string>(null),
    first_enter: new BehaviorSubject<boolean>(null),
    system_data: new BehaviorSubject<System>(null),
    admin_data_list: new BehaviorSubject<Admin[]>(null),
    customer_data_list: new BehaviorSubject<Customer[]>(null),
    partner_data_list: new BehaviorSubject<Partner[]>(null),
    reservation_timeslot_data_list: new BehaviorSubject<Reservation_Timeslot[]>(null),
    appointment_data_list: new BehaviorSubject<Appointment[]>(null),
    banner_data_list: new BehaviorSubject<Banner[]>(null),
    question_data_list: new BehaviorSubject<Question[]>(null),
    response_data_list: new BehaviorSubject<Response[]>(null),
    notification_data_list: new BehaviorSubject<Notification[]>(null),
    product_data_list: new BehaviorSubject<Product[]>(null),
    customer_order_data: new BehaviorSubject<Order>(null),
    order_data_list: new BehaviorSubject<Order[]>(null),
    partner_design_order_data: new BehaviorSubject<Design_Order>(null),
    design_order_data_list: new BehaviorSubject<Design_Order[]>(null),
    activity_data_list: new BehaviorSubject<Activity[]>(null)
  }

  constructor(
    private storage: Storage,
    private httpClient: HttpClient,
    private util: UtilService,
    private localNotification: LocalNotificationService,
    // private auth: AuthService
  ) {
    // this.init();
  }

  async init() {
    // If using, define drivers here: await this.storage.defineDriver(/*...*/);
    const storage = await this.storage.create();
    this._storage = storage;

    if (environment.resetDatabaseOnInit) {
      const clearStorage = await this.storage.clear();
    }
    // console.log(new Date().toISOString());
    const first_enter = await this.get(Local_Storage.first_enter);
    console.log("first_enter: ", first_enter);
    const data_last_updated = await this.get(Local_Storage.data_last_updated);
    console.log("data_last_updated: ", data_last_updated);
    this.localStorageToState();
    setTimeout(() => {

      this.httpClient.get("assets/data.json").subscribe(async data => {
        if ((first_enter == null || first_enter == true) || environment.resetDatabaseOnInit
          || data_last_updated == null || new Date(data_last_updated) < new Date(data['data_last_updated'])) {
          console.log("load data from data.json");

          Object.values(data).forEach(async (d, index) => {
            const key = Object.keys(data)[index]
            // console.log(key);
            if (key != 'data_last_updated') {
              let d2 = d.map(r => {
                r.create_datetime = new Date()
                if (r.data_type == data_type.reservation_timeslot_data) {
                  r.date = new Date(r.date)
                }
                return r
              })
              this.set(key, d).then(async () => {
                this.LOCAL_STORE[key].next(d);
              });
            }
          });

          this.LOCAL_STORE.first_enter.next(false);
          this.set(Local_Storage.first_enter, false);
          this.LOCAL_STORE.data_last_updated.next(data['data_last_updated']);
          this.set(Local_Storage.data_last_updated, data['data_last_updated']);
        }
        else {
          Object.values(this.LOCAL_STORE).forEach(async (d, index) => {
            const key = Object.keys(this.LOCAL_STORE)[index]
            this.get(key).then(value => {
              this.LOCAL_STORE[key].next(value);
            });
          });

        }
      });

    }, 0);

    setTimeout(() => {
      if (!this.LOCAL_STORE.system_data.value) {
        this.initSystemData();
      }
    }, 1000);
  }

  initSystemData() {
    const system_data: System = {
      customer_vip_threshold: null,
      design_product_list: [
        {
          product_type: Design_Product_Type.tshirt,
          attribute: "S碼",
          price: '200'
        },
        {
          product_type: Design_Product_Type.tshirt,
          attribute: "M碼",
          price: '220'
        },
        {
          product_type: Design_Product_Type.tshirt,
          attribute: "L碼",
          price: '240'
        },
        {
          product_type: Design_Product_Type.phone_case,
          attribute: "iphone 12",
          price: '350'
        }
      ],
      commission_rate_per_desgin_order: 20
    }
    this.LOCAL_STORE.system_data.next(system_data);
  }

  // Create and expose methods that users of this service can
  // call, for example:
  public async set(key: string, value: any): Promise<any> {
    return await this._storage?.set(key, value);
  }

  public async get(key: string): Promise<any> {
    return await this._storage.get(key);
  }

  localStorageToState() {
    Object.values(this.LOCAL_STORE).forEach(async (state$: BehaviorSubject<any>, index) => {
      const key = Object.keys(this.LOCAL_STORE)[index] as Local_Storage;
      const value = await this.get(key);
      // console.log(`${key}: `, value);
      if (value) state$.next(value);
      state$.pipe(
        // tap(res => {
        // console.log(key, res)
        // }),
        skip(1)
      ).subscribe(data => {
        // console.log(`state ${key}: `, data);
        this.set(key, data).then(async () => {
          const test = await this.get(key);
          // console.log(`storage ${key}: `, test);
        });
      }
      )
    })
  }

  createInitData(dt) {
    const getRandomInt = (max) => {
      return Math.floor(Math.random() * max);
    }

    console.log(dt);
    let random_id = getRandomInt(1000).toString();
    let data = null;
    switch (dt) {
      case data_type.customer_data:
        const customer_data: Customer = {
          id: random_id,
          data_type: data_type.customer_data,
          create_datetime: new Date(),
          status: General_Status.created,
          username: environment.production ? '' : `cusomter_${random_id}`,
          email: environment.production ? '' : `customer_${random_id}@gmail.com`,
          phone: environment.production ? '' : "61234564",
          display_name: environment.production ? '' : `Customer ${random_id}`,
          // month_of_birth: environment.production ? '' : "11",
          password: environment.production ? '' : "123456789",
          is_vip: false,
          profile_img: environment.production ? '' : "https://i.picsum.photos/id/1045/200/200.jpg?hmac=NOMPYGOtm89-zlf7NNDG7qSjCOy3XpvrdQRBF4aUZgE",
          credit: 0,
          referral_code: this.util.genereateRandomCode(5),
          referrer_code: ''
        }
        data = customer_data;
        break;
      case data_type.partner_data:
        const partner_data: Partner = {
          id: random_id,
          data_type: data_type.partner_data,
          create_datetime: new Date(),
          status: General_Status.created,
          username: environment.production ? '' : `partner_${random_id}`,
          email: environment.production ? '' : `partner_${random_id}@gmail.com`,
          phone: environment.production ? '' : "64524561",
          display_name: environment.production ? '' : `Partner ${random_id}`,
          // month_of_birth: environment.production ? '' : "11",
          is_staff: true,
          password: environment.production ? '' : "123456789",
          profile_img: environment.production ? '' : "https://i.picsum.photos/id/1045/200/200.jpg?hmac=NOMPYGOtm89-zlf7NNDG7qSjCOy3XpvrdQRBF4aUZgE"
        }
        data = partner_data;
        break;
      case data_type.reservation_timeslot_data:
        const reservation_timeslot_data: Reservation_Timeslot = {
          id: random_id,
          data_type: data_type.reservation_timeslot_data,
          create_datetime: new Date(),
          status: General_Status.active,
          service_name: "寵物美容",
          date: new Date(),
          start_time: environment.production ? '' : "09:00",
          end_time: environment.production ? '' : "17:40",
          quota: environment.production ? 1 : 1,
          price: environment.production ? '0' : "0",
          vip_price: environment.production ? '0' : '0'
        }
        data = reservation_timeslot_data;
        break;
      case data_type.appointment_data:
        const appointment_data: Appointment = {
          id: random_id,
          data_type: data_type.appointment_data,
          create_datetime: new Date(),
          status: Appointment_Status.created,
          reservation_timeslot_id: null,
          customer_id: null,
          partner_id: null,
          remark: "",
          ranking: null
        }
        data = appointment_data;
        break;
      case data_type.banner_data:
        const banner_data: Banner = {
          id: random_id,
          data_type: data_type.banner_data,
          create_datetime: new Date(),
          status: General_Status.created,
          img_url: ""
        }
        data = banner_data;
        break;
      case data_type.question_data:
        const question_data: Question = {
          id: random_id,
          data_type: data_type.question_data,
          create_datetime: new Date(),
          status: General_Status.created,
          title: "",
          description: "",
          options: [],
          multiple: false
        }
        data = question_data;
        break;
      case data_type.notification_data:
        const notification_data: Notification = {
          id: random_id,
          data_type: data_type.notification_data,
          create_datetime: new Date(),
          status: General_Status.created,
          customer_id_list: [],
          title: "",
          content: "",
        }
        data = notification_data;
        break;
      case data_type.product_data:
        const product_data: Product = {
          id: random_id,
          data_type: data_type.product_data,
          create_datetime: new Date(),
          status: General_Status.created,
          categories: [],
          name: `product ${random_id}`,
          description: `description ${random_id}`,
          attributes: [],
          price: '0',
          vip_price: '0',
          product_img_url_list: ["https://i.picsum.photos/id/1045/200/200.jpg?hmac=NOMPYGOtm89-zlf7NNDG7qSjCOy3XpvrdQRBF4aUZgE"],
          inventory: 0,
          redeemable: false,
          credit: 0
        }
        data = product_data;
        break;
      case data_type.order_data:
        const order_data: Order = {
          id: random_id,
          data_type: data_type.order_data,
          create_datetime: new Date(),
          status: Order_Status.pending,
          customer_id: null,
          cart: [],
          subtotal: 0,
          shipping_fee: 0,
          discount: 0,
          total_amount: 0,
          total_credit: 0,
          payment_data: {
            payment_method: 'credit_card',
            payment_datetime: new Date(),
            bank_transfer_img: '',
            card_number: '',
            card_holder: '',
            expiry_date: '',
            cvv: '',
            payment_data: null
          },
          delivery_data: {
            delivery_method: 'store_pickup',
            delivery_address: ''
          },
          is_vip: false,
          is_redeemption: false
        }
        data = order_data;
        break;
      case data_type.design_order_data:
        const design_order_data: Design_Order = {
          id: random_id,
          data_type: data_type.design_order_data,
          create_datetime: new Date(),
          status: Order_Status.pending,
          customer_id: null,
          customer_phone: null,
          customer_name: null,
          customer_email: null,
          partner_id: null,
          design_data: {
            "background": "../assets/icon/tshirt.svg",
            "background_color": "#000000",
            "dashboard_color": "#ffffff",
            "items": [],
            "final_product_image": ""
          },
          product_type: Design_Product_Type.tshirt,
          product_detail: '',
          design_product: null,
          quantity: 1,
          subtotal: 0,
          shipping_fee: 0,
          discount: 0,
          total_amount: 0,
          commission: 0,
          payment_data: {
            payment_method: 'credit_card',
            payment_datetime: new Date(),
            bank_transfer_img: '',
            card_number: '',
            card_holder: '',
            expiry_date: '',
            cvv: '',
            payment_data: null
          },
          delivery_data: {
            delivery_method: 'store_pickup',
            delivery_address: ''
          },
          signature_image: null,
          quotation_html: ''
        }
        data = design_order_data;
        break;
      case data_type.activity_data:
        const activity_data: Activity = {
          id: random_id,
          data_type: data_type.activity_data,
          create_datetime: new Date(),
          title: null,
          partner_id: null,
        }
        data = activity_data;
        break;

      default:
        break;
    }
    return data;
  }

  saveToDatabase(type: 'edit' | 'read' | 'create', dt, data) {
    // console.log(type);
    // console.log(dt);
    // console.log(data);
    console.log(this.LOCAL_STORE[Data_Type_Mapping_Local_Storage[dt]]);
    let original_data_list: any[] = this.LOCAL_STORE[Data_Type_Mapping_Local_Storage[dt]].value;
    console.log(original_data_list);
    if (original_data_list == null) {
      original_data_list = [];
    }

    let data_before_edit = null;
    if (type == 'edit') {
      let index = original_data_list.findIndex(d => d.id == data.id);
      data_before_edit = JSON.parse(JSON.stringify(original_data_list[index]));
      if (index != -1) original_data_list[index] = data;
    } else if (type == 'create') {
      original_data_list.push(data);
    }
    this.LOCAL_STORE[Data_Type_Mapping_Local_Storage[dt]].next(original_data_list);

    if (dt == data_type.order_data) {
      let order_data = data as Order;
      if (type == 'create') {
        if (order_data.status == Order_Status.paid) {
          if (order_data.total_amount > 0) {
            this.assignCreditToCustomer(order_data.total_amount, order_data.customer_id);
          }
        }
      }
      else if (type == 'edit') {
        let order_data_before_edit = data_before_edit as Order;
        if (order_data_before_edit.status != Order_Status.paid && order_data.status == Order_Status.paid) {
          if (order_data.total_amount > 0) {
            this.assignCreditToCustomer(order_data.total_amount, order_data.customer_id);
          }
        }
      }

    }

    if (dt == data_type.appointment_data) {
      let appointment_data = data as Appointment;
      appointment_data['reservation_timeslot_data'] = this.LOCAL_STORE.reservation_timeslot_data_list.value.find((r: Reservation_Timeslot) => r.id == appointment_data.reservation_timeslot_id);
      if (type == 'create') {
        this.localNotification.createAppointmentNotification(appointment_data);
      }
      else if (type == 'edit') {
        let appointment_data_before_edit = data_before_edit as Appointment;
        if (appointment_data_before_edit.status != Appointment_Status.cancelled && appointment_data.status == Appointment_Status.cancelled) {
          this.localNotification.cancelAppointmentNotification(appointment_data);
        }
      }
    }

    console.log(this.LOCAL_STORE[Data_Type_Mapping_Local_Storage[dt]].value);
  }

  assignCreditToCustomer(credit_amount, customer_id: string) {
    if (customer_id != null && customer_id != '') {
      let customer_data = this.LOCAL_STORE.customer_data_list.value.find((c: Customer) => c.id == customer_id);
      if (customer_data) {
        customer_data.credit += Math.round(credit_amount);
        this.saveToDatabase('edit', customer_data.data_type, customer_data);
        // if (this.authService.userData.value != null && this.authService.userData.value.id == customer_data.id){
        // this.authService.userData.next(customer_data);
        // }
      }
    }
  }


}
