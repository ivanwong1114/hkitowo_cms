import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { CalendarMode, Step } from 'ionic2-calendar/calendar';
import { Activity, Activity_Title, Appointment, Appointment_Status, Customer, data_type, Partner, Reservation_Timeslot } from 'src/app/interface';
import { DataService } from 'src/app/services/data.service';
import { StorageService } from 'src/app/services/storage.service';
import { UtilService } from 'src/app/services/util.service';
import { CalendarComponent } from "ionic2-calendar";
import { distinctUntilChanged } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.page.html',
  styleUrls: ['./appointment.page.scss'],
})
export class AppointmentPage implements OnInit {
  segment_value: 'appointment' | 'appointment_record' = 'appointment'
  my_appointments = null;
  seleted_ranking = 0;

  @ViewChild(CalendarComponent, { static: false }) myCalendar: CalendarComponent;

  eventSource;
  viewTitle;

  isToday: boolean;
  calendar = {
    mode: 'month' as CalendarMode,
    step: 30 as Step,
    currentDate: new Date(),
    dateFormatter: {
      formatMonthViewDay: function (date: Date) {
        return date.getDate().toString();
      },
      formatMonthViewDayHeader: function (date: Date) {
        return 'MonMH';
      },
      formatMonthViewTitle: function (date: Date) {
        return 'testMT';
      },
      formatWeekViewDayHeader: function (date: Date) {
        return 'MonWH';
      },
      formatWeekViewTitle: function (date: Date) {
        return 'testWT';
      },
      formatWeekViewHourColumn: function (date: Date) {
        return 'testWH';
      },
      formatDayViewHourColumn: function (date: Date) {
        return 'testDH';
      },
      formatDayViewTitle: function (date: Date) {
        return 'testDT';
      }
    }
  };

  reservationSub$$: Subscription = null;
  appointmentSub$$: Subscription = null;

  ava_reservation_timeslot_data_list = null;

  user_data = this.auth.userData.value;

  public get AppointmentStatus(): typeof Appointment_Status {
    return Appointment_Status;
  }
  constructor(
    private storageService: StorageService,
    private dataService: DataService,
    private alertController: AlertController,
    private util: UtilService,
    private modalController: ModalController,
    public auth: AuthService
  ) { }

  ngOnInit() {
    if (this.auth.userData.value != null){
      this.my_appointments = this.dataService.getCustomerAppointmentWithReservationData();
      console.log(this.my_appointments);
    }
      this.reservationSub$$ = this.storageService.LOCAL_STORE.reservation_timeslot_data_list
        .pipe(distinctUntilChanged())
        .subscribe(() => {
          this.loadAvailableReservationTimeslotDataList();
        });
      this.appointmentSub$$ = this.storageService.LOCAL_STORE.appointment_data_list
        .pipe(distinctUntilChanged())
        .subscribe(() => {
          this.loadAvailableReservationTimeslotDataList();
        });

  }

  ionViewDidEnter(){}

  ngOnDestory() {
    this.reservationSub$$.unsubscribe();
    this.appointmentSub$$.unsubscribe();
  }

  segmentChanged($event) {
    // console.log($event);
    this.segment_value = $event.detail.value;
    if (this.segment_value == 'appointment_record'){
      this.my_appointments = this.dataService.getCustomerAppointmentWithReservationData();
    }
    else{
      this.loadAvailableReservationTimeslotDataList();
    }
  }

  async presentCancelAlert(appointment: Appointment) {
    const alert = await this.alertController.create({
      // cssClass: 'my-custom-class',
      header: '確認取消預約？',
      // message: '確認取消預約？',
      buttons: [
        {
          text: '否',
          role: 'cancel',
          cssClass: 'secondary',
          id: 'cancel-button',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '是',
          id: 'confirm-button',
          handler: () => {
            this.cancelAppointment(appointment);
          }
        }
      ]
    });

    await alert.present();
  }

  async presentAppointmentAlert(reservation_timeslot_data: Reservation_Timeslot) {
    if (this.auth.userData.value == null){
      return this.util.presentToast('請先登入');
    }
    const alert = await this.alertController.create({
      // cssClass: 'my-custom-class',
      header: '確認預約？',
      message: `服務：${reservation_timeslot_data.service_name}<br>日期：${new Date(reservation_timeslot_data.date).toISOString().split('T')[0]}<br>時間：${reservation_timeslot_data.start_time}-${reservation_timeslot_data.end_time}<br>價格：$${reservation_timeslot_data.price}`,
      buttons: [
        {
          text: '否',
          role: 'cancel',
          cssClass: 'secondary',
          id: 'cancel-button',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '是',
          id: 'confirm-button',
          handler: () => {
            this.confirmAppointment(reservation_timeslot_data);
          }
        }
      ]
    });

    await alert.present();
  }

  cancelAppointment(appointment: Appointment) {
    appointment.status = Appointment_Status.cancelled;
    this.storageService.saveToDatabase('edit', appointment.data_type, appointment);

    let activity_data: Activity = this.storageService.createInitData(data_type.activity_data);
    activity_data.title = Activity_Title.change_appointment_status;
    activity_data.partner_id = this.auth.userData.value.id;
    this.storageService.saveToDatabase('create', data_type.activity_data, activity_data);

    this.util.presentToast("已取消預約");
  }

  confirmAppointment(reservation_timeslot_data: Reservation_Timeslot){
    let new_appointment_data: Appointment = this.storageService.createInitData(data_type.appointment_data);
    new_appointment_data.customer_id = this.auth.userData.value.id;
    new_appointment_data.reservation_timeslot_id = reservation_timeslot_data.id;
    new_appointment_data.status = Appointment_Status.created;
    this.storageService.saveToDatabase('create', new_appointment_data.data_type, new_appointment_data);
    this.loadAvailableReservationTimeslotDataList();
  }

  rank(appointment: Appointment) {
    appointment.ranking = this.seleted_ranking;
    this.storageService.saveToDatabase('edit', appointment.data_type, appointment);
    this.seleted_ranking = 0;
    this.modalController.dismiss();
  }

  onViewTitleChanged(title) {
    console.log(title);
    this.viewTitle = title;
  }

  onEventSelected(event) {
    console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
  }

  changeMode(mode) {
    this.calendar.mode = mode;
  }

  today() {
    this.calendar.currentDate = new Date();
  }

  onTimeSelected(ev) {
    console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
      (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
  }

  slide(type: 'next' | 'previous') {
    if (type == 'next') {
      this.myCalendar.slideNext();
    }
    else {
      this.myCalendar.slidePrev();
    }
  }

  onCurrentDateChanged(event: Date) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    event.setHours(0, 0, 0, 0);
    this.isToday = today.getTime() === event.getTime();
  }

  loadAvailableReservationTimeslotDataList() {

    // let reservation_timeslot_data_list: Reservation_Timeslot[] = this.storageService.LOCAL_STORE.reservation_timeslot_data_list.value;
    // let appointment_data_list: Appointment[] = this.storageService.LOCAL_STORE.appointment_data_list.value;
    // let customer_data_list: Customer[] = this.storageService.LOCAL_STORE.customer_data_list.value;
    // let partner_data_list: Partner[] = this.storageService.LOCAL_STORE.partner_data_list.value;
    // // console.log(reservation_timeslot_data_list);
    // // console.log(appointment_data_list);
    // if (reservation_timeslot_data_list && appointment_data_list) {
    //   reservation_timeslot_data_list.forEach((r: Reservation_Timeslot) => {
    //     r['appointments'] =
    //       appointment_data_list
    //         .filter((a: Appointment) => a.reservation_timeslot_id == r.id)
    //         .map(a => {
    //           a['customer'] = customer_data_list.find(d => d.id == a.customer_id);
    //           a['partner'] = partner_data_list.find(d => d.id == a.partner_id);
    //           return a;
    //         });
    //   });
    //   this.addEvent(reservation_timeslot_data_list);
    // }

    // this.ava_reservation_timeslot_data_list = this.dataService.getAvailableReservationTimeslot();
    // console.log(this.ava_reservation_timeslot_data_list);
    // setTimeout(() => {
      this.addEvent(this.dataService.getAvailableReservationTimeslot());
    // }, 500);
  }

  addEvent(data_list: any[]) {
    // var events = [];
    // data_list.forEach((data: Reservation_Timeslot) => {
    //   let dt = new Date(data.date);
    //   dt.setHours(0, 0, 0, 0);
    //   dt.setMinutes(dt.getMinutes() + dt.getTimezoneOffset());
    //   let dt2 = new Date(data.date);
    //   dt2.setHours(0, 0, 0, 0);
    //   dt2.setMinutes(dt2.getMinutes() + dt2.getTimezoneOffset());

    //   let startTimeSecond = (+data.start_time.split(':')[0]) * 60 * 60 + (+data.start_time.split(':')[1]) * 60 + (+"00");
    //   let startTime = dt.setSeconds(dt.getSeconds() + startTimeSecond + (8 * 60 * 60));
    //   let endTimeSecond = (+data.end_time.split(':')[0]) * 60 * 60 + (+data.end_time.split(':')[1]) * 60 + (+"00");
    //   let endTime = dt2.setSeconds(dt2.getSeconds() + endTimeSecond + (8 * 60 * 60));
    //   // console.log(data.date);
    //   // console.log(data.start_time);
    //   // console.log(startTime);
    //   // console.log(new Date(startTime));

    //   // console.log(data['appointments']);
    //   data['appointments'].forEach((appointment: Appointment) => {
    //     events.push({
    //       title: `Quota: ${data.quota}, Price: $${data.price}, Customer: ${appointment['customer'].display_name}, Partner: ${appointment['partner'].display_name}`,
    //       startTime: new Date(startTime),
    //       endTime: new Date(endTime),
    //       allDay: false
    //     });
    //   });
    //   this.eventSource = events;
    // });

    console.log(data_list);

    var events = [];
    data_list.forEach((data: Reservation_Timeslot) => {
      let dt = new Date(data.date);
      dt.setHours(0, 0, 0, 0);
      dt.setMinutes(dt.getMinutes() + dt.getTimezoneOffset());
      let dt2 = new Date(data.date);
      dt2.setHours(0, 0, 0, 0);
      dt2.setMinutes(dt2.getMinutes() + dt2.getTimezoneOffset());

      let startTimeSecond = (+data.start_time.split(':')[0]) * 60 * 60 + (+data.start_time.split(':')[1]) * 60 + (+"00");
      let startTime = dt.setSeconds(dt.getSeconds() + startTimeSecond + (8 * 60 * 60));
      let endTimeSecond = (+data.end_time.split(':')[0]) * 60 * 60 + (+data.end_time.split(':')[1]) * 60 + (+"00");
      let endTime = dt2.setSeconds(dt2.getSeconds() + endTimeSecond + (8 * 60 * 60));
      // console.log(data.date);
      // console.log(data.start_time);
      // console.log(startTime);
      // console.log(new Date(startTime));

      // console.log(data['appointments']);

      events.push({
        // title: `Quota: ${data.quota}, Price: $${data.price}, Customer: ${appointment['customer'].display_name}, Partner: ${appointment['partner'].display_name}`,
        startTime: new Date(startTime),
        endTime: new Date(endTime),
        allDay: false,
        reservation_timeslot_data: data 
      });
      this.eventSource = events;
    });

  }

}
