import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DesignPageRoutingModule } from './design-routing.module';

import { DesignPage } from './design.page';
import { SharedModule } from 'src/app/shared.module';
import { MaterialModule } from 'src/app/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DesignPageRoutingModule,
    SharedModule,
    MaterialModule
  ],
  declarations: [DesignPage]
})
export class DesignPageModule {}
