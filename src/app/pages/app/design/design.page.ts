import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import { NavigationOptions } from '@ionic/angular/providers/nav-controller';
import { Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import { Design_Order, Order } from 'src/app/interface';
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/services/data.service';
import { StorageService } from 'src/app/services/storage.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-design',
  templateUrl: './design.page.html',
  styleUrls: ['./design.page.scss'],
})
export class DesignPage implements OnInit {

  design_order_data_list = this.storageService.LOCAL_STORE.design_order_data_list.value;

  my_orders: Design_Order[] = null;

  designOrderSub$$: Subscription = null;


  constructor(
    private storageService: StorageService,
    private dataService: DataService,
    private alertController: AlertController,
    private util: UtilService,
    private modalController: ModalController,
    private auth: AuthService,
    private nav: NavController
  ) { }

  ngOnInit() {
    // if (this.auth.userData.value == null){
    //   return this.nav.navigateRoot('/tabs/shopping');
    // }
    setTimeout(() => {
      // this.storageService.LOCAL_STORE.partner_design_order_data.next(null);

      this.my_orders = this.dataService.getPartnerDesignOrder();
      this.my_orders[0].design_data.final_product_image = '';
      this.my_orders[0].signature_image = '';
      this.my_orders[0].quotation_html = '';

      console.log(JSON.stringify(this.my_orders[0]));
      // if (this.my_orders){
      //   this.my_orders.map((o: Order) => {
      //     let init = 0;
      //     o['total_quantity'] = o.cart.map(d => d.quantity).reduce( (previousValue, currentValue) => previousValue + currentValue, init);
      //     return
      //   });
      // }
  
      this.designOrderSub$$ = this.storageService.LOCAL_STORE.design_order_data_list
        .pipe(distinctUntilChanged())
        .subscribe(() => {
          this.my_orders = this.dataService.getPartnerDesignOrder();
        });
    }, 500);

  }

  ngOnDestory() {
    this.designOrderSub$$.unsubscribe();
  }

  goOrderDetail(order: Order){
    let options: NavigationOptions = {
      //   replaceUrl: options?.replaceUrl,
      //   skipLocationChange: options?.skipLocationChange,
        state: order
    }
    this.nav.navigateForward('order-detail', options);
  }

}
