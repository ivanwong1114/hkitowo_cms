import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PartnerAppointmentPage } from './partner-appointment.page';

const routes: Routes = [
  {
    path: '',
    component: PartnerAppointmentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PartnerAppointmentPageRoutingModule {}
