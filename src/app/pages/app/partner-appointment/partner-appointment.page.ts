import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Appointment_Status } from 'src/app/interface';
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/services/data.service';
import { StorageService } from 'src/app/services/storage.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-partner-appointment',
  templateUrl: './partner-appointment.page.html',
  styleUrls: ['./partner-appointment.page.scss'],
})
export class PartnerAppointmentPage implements OnInit {

  my_appointments = null;
  public get AppointmentStatus(): typeof Appointment_Status {
    return Appointment_Status;
  }
  constructor(
    private storageService: StorageService,
    private dataService: DataService,
    private alertController: AlertController,
    private util: UtilService,
    private modalController: ModalController,
    private auth: AuthService
  ) { }

  ngOnInit() {
    this.my_appointments = this.dataService.getPartnerAppointmentWithReservationData();
    console.log(this.my_appointments);

    // this.reservationSub$$ = this.storageService.LOCAL_STORE.reservation_timeslot_data_list
    //   .pipe(distinctUntilChanged())
    //   .subscribe(() => {
    //     this.loadAvailableReservationTimeslotDataList();
    //   });
    // this.appointmentSub$$ = this.storageService.LOCAL_STORE.appointment_data_list
    //   .pipe(distinctUntilChanged())
    //   .subscribe(() => {
    //     this.loadAvailableReservationTimeslotDataList();
    //   });
  }

}
