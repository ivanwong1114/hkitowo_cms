import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PartnerAppointmentPageRoutingModule } from './partner-appointment-routing.module';

import { PartnerAppointmentPage } from './partner-appointment.page';
import { SharedModule } from 'src/app/shared.module';
import { MaterialModule } from 'src/app/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PartnerAppointmentPageRoutingModule,
    SharedModule,
    MaterialModule
  ],
  declarations: [PartnerAppointmentPage]
})
export class PartnerAppointmentPageModule {}
