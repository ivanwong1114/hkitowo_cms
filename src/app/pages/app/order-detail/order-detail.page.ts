import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Order } from 'src/app/interface';
import { OrderService } from 'src/app/services/order.service';
import { StateService } from 'src/app/services/state.service';
import { StorageService } from 'src/app/services/storage.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.page.html',
  styleUrls: ['./order-detail.page.scss'],
})
export class OrderDetailPage implements OnInit {

  order_data: Order = null;

  constructor(
    private router: Router,
    public state: StateService,
    private orderService: OrderService,
    private storageService: StorageService
  ) { 
    if (router.getCurrentNavigation().extras.state) {
      let state: any = this.router.getCurrentNavigation().extras.state;
      this.order_data = state;
    }
  }

  ngOnInit() {

    if (environment.production == false && this.order_data == null){
      this.order_data = this.storageService.LOCAL_STORE.order_data_list.value[0];
    }
    console.log(this.order_data);
  }

}
