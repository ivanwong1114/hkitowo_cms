import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Cart, Order } from 'src/app/interface';
import { AuthService } from 'src/app/services/auth.service';
import { OrderService } from 'src/app/services/order.service';
import { StateService } from 'src/app/services/state.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-shopping-bag',
  templateUrl: './shopping-bag.page.html',
  styleUrls: ['./shopping-bag.page.scss'],
})
export class ShoppingBagPage implements OnInit {

  customer_order_data: Order = null;

  user_data = this.auth.userData.value;

  constructor(
    private storageService: StorageService,
    private state: StateService,
    public orderService: OrderService,
    private cdf: ChangeDetectorRef,
    private auth: AuthService,
    private nav: NavController
  ) { }

  ngOnInit() {
    this.customer_order_data = this.storageService.LOCAL_STORE.customer_order_data.value;
    
    if (this.customer_order_data){
      this.customer_order_data.cart.forEach((c: Cart) => {
        c.product_data['attributes_display'] = c.product_data.attributes.join('/ ');
      });
    }
    console.log(this.customer_order_data);

  }


  getCheckoutPage(){
    this.nav.navigateForward('checkout');
  }

}
