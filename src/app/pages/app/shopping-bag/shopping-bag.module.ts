import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShoppingBagPageRoutingModule } from './shopping-bag-routing.module';

import { ShoppingBagPage } from './shopping-bag.page';
import { SharedModule } from 'src/app/shared.module';
import { MaterialModule } from 'src/app/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShoppingBagPageRoutingModule,
    SharedModule,
    MaterialModule
  ],
  declarations: [ShoppingBagPage]
})
export class ShoppingBagPageModule {}
