import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { IonContent, NavController } from '@ionic/angular';
import { Order, Order_Status } from 'src/app/interface';
import { AuthService } from 'src/app/services/auth.service';
import { OrderService } from 'src/app/services/order.service';
import { StateService } from 'src/app/services/state.service';
import { StorageService } from 'src/app/services/storage.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {

  customer_order_data: Order = null;
  @ViewChild('upload_img', { static: false }) upload_img: ElementRef;
  @ViewChild(IonContent, { static: false }) ion_content: IonContent;

  constructor(
    private storageService: StorageService,
    private state: StateService,
    public orderService: OrderService,
    private auth: AuthService,
    private nav: NavController,
    private util: UtilService
  ) { }

  ngOnInit() {
    this.customer_order_data = this.storageService.LOCAL_STORE.customer_order_data.value;
  }

  paymentMethodChange($event){
    // console.log($event);
    if ($event.detail.value == 'credit_card' || $event.detail.value == 'bank_transfer'){
      this.customer_order_data.payment_data.payment_method = $event.detail.value;
    }

  }

  deliveryMethodChange($event){
    // console.log($event);
    // if ($event.detail.value == 'store_pickup' || $event.detail.value == 'sf_delivery'){
    //   this.customer_order_data.delivery_data.delivery_method = $event.detail.value;
    // }
    setTimeout(() => {
      this.ion_content.scrollToBottom(300);
    }, 100);
  }


  triggerImgUpload() {
    if (this.upload_img == null) {
      return;
    }
    this.upload_img.nativeElement.click();
  }
  uploadImg() {
    if (this.upload_img == null || this.util.isLoading) {
      return;
    }
    const fileList: FileList = this.upload_img.nativeElement.files;
    if (fileList && fileList.length > 0) {
      this.util.firstFileToBase64(fileList[0]).then(async (base64: string) => {
        console.log(base64);
        this.customer_order_data.payment_data.bank_transfer_img = base64;
      })
    }
  }
  
  completeCheckout(){

    const credit_card_pattern = new RegExp('^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$');
    const expiry_month_year_pattern = new RegExp('^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})$');
    const cvv_pattern = new RegExp('^[0-9]{3,4}$');

    console.log(this.customer_order_data);
    if (this.customer_order_data.is_redeemption == false){
      if (this.customer_order_data.payment_data.payment_method == 'credit_card'){
        if (credit_card_pattern.test(this.customer_order_data.payment_data.card_number) == false){
          return this.util.presentToast("請輸入正確的信用卡號碼");
        }
        if (expiry_month_year_pattern.test(this.customer_order_data.payment_data.expiry_date) == false){
          return this.util.presentToast("請輸入正確的信用卡到期月份年份");
        }
        if (cvv_pattern.test(this.customer_order_data.payment_data.cvv) == false){
          return this.util.presentToast("請輸入正確的cvv");
        }
      }
      else if(this.customer_order_data.payment_data.payment_method == 'bank_transfer'){
        if (this.customer_order_data.payment_data.bank_transfer_img == null || this.customer_order_data.payment_data.bank_transfer_img == ''){
          return this.util.presentToast("請上載銀行轉帳證明");
        }
      }
    }
    //credit
    else{
      this.customer_order_data.payment_data.payment_method == null;
      this.customer_order_data.status = Order_Status.paid;
    }


    if (this.customer_order_data.delivery_data.delivery_method == 'sf_delivery'){
      if (this.customer_order_data.delivery_data.delivery_address == ''){
        return this.util.presentToast("請輸入送貨地址");
      }
    }

    this.util.presentLoading();

      //TODO payment processing
    if (this.customer_order_data.payment_data.payment_method == 'credit_card'){
      this.customer_order_data.status = Order_Status.paid;
    }
    else if (this.customer_order_data.payment_data.payment_method == 'bank_transfer'){
      this.customer_order_data.status == Order_Status.pending_verification;
    }
    
    setTimeout(() => {
      this.storageService.LOCAL_STORE.customer_order_data.next(this.customer_order_data);
      this.orderService.createNewOrder();
      this.util.dismissLoading();
      this.nav.navigateRoot('checkout-completed');
    }, 1500);
  }

}
