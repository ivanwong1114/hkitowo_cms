import { CdkDragDrop, CdkDragMove, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, ElementRef, NgZone, OnInit, QueryList, Renderer2, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController, AlertController, IonSelect, NavController, Platform, PopoverController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { UtilService } from 'src/app/services/util.service';
import * as htmlToImage from 'html-to-image';
import { toPng, toJpeg, toBlob, toPixelData, toSvg } from 'html-to-image';
import { StateService } from 'src/app/services/state.service';
import { Font } from 'ngx-font-picker';
import { environment } from 'src/environments/environment';
import { StorageService } from 'src/app/services/storage.service';
import { OrderService } from 'src/app/services/order.service';
import { Design_Product_Type } from 'src/app/interface';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  user_data = this.auth.userData.value;

  partner_design_order_data = this.storageService.LOCAL_STORE.partner_design_order_data.value;

  disabledDrag = false;
  currentDeg = 0;
  currentFlipX = 1;
  currentFlipY = 1;
  currentScale = 1;

  dragPosition = { x: 0, y: 0 };

  selected_image_id: number;
  selected_image_index: number;

  current_redo_undo: number = 0;
  redo_undo_list = new Array(10);

  redo_list = [];
  undo_list = [];

  screen_capture_mode = false;
  preview_mode = false;
  layer_menu_hidden = false;

  windowWidth = window.innerWidth;

  @ViewChild("layer_menu", { static: false }) layer_menu: ElementRef;

  @ViewChild('upload_img', { static: false }) upload_img: ElementRef;

  @ViewChild('mySelect', { static: false }) selectRef: IonSelect;


  constructor(
    private renderer: Renderer2,
    private util: UtilService,
    private actionSheetCtrl: ActionSheetController,
    // private camera: Camera,
    private nav: NavController,
    private popoverController: PopoverController,
    private alertController: AlertController,
    // private screenshot: Screenshot,
    // private file: File,
    // private base64ToGallery: Base64ToGallery,
    // private webview: WebView,
    // private base64: Base64,
    private platform: Platform,
    // private commonService: CommonService,
    // private dataService: ServerDataService,
    // private route: ActivatedRoute,
    private router: Router,
    // private event: Events,
    // private languageService: LanguageService,
    private state: StateService,
    private ngZone: NgZone,
    private auth: AuthService,
    private storageService: StorageService,
    private orderService: OrderService

  ) {
  }

  ngOnInit() { 

    if (this.partner_design_order_data == null){
      this.orderService.checkPartnerOrderData();
      this.partner_design_order_data = this.storageService.LOCAL_STORE.partner_design_order_data.value;
      setTimeout(() => {

        if (!environment.production){
          this.partner_design_order_data.design_data.items = [
            {
              "id": 1,
              "is_image": true,
              "image": "../assets/template/cat2.svg",
              "width": 150,
              "height": null,
              "position": { "x": 0, "y": 0 },
              "angle": 0,
              "flip_x": 1,
              "flip_y": 1,
              "z_index": 2,
              "background_color": "transparent",
              "text": "",
              "text_color": "#000000",
              "font": null
            },
            {
              "id": 2,
              "is_image": true,
              "image": "../assets/template/cat3.svg",
              "width": 80,
              "height": null,
              "position": { "x": 0, "y": 0 },
              "angle": 0,
              "flip_x": 1,
              "flip_y": 1,
              "z_index": 1,
              "background_color": "transparent",
              "text": "",
              "text_color": "#000000",
              "font": null
            },
            {
              "id": 3,
              "is_image": false,
              "image": "",
              "width": 120,
              "height": null,
              "position": { "x": 0, "y": 0 },
              "angle": 0,
              "flip_x": 1,
              "flip_y": 1,
              "z_index": 1,
              "background_color": "transparent",
              "text": "輸入文字",
              "text_color": "#000000",
              "font": null
            }
          ];
        }
      }, 1000);
    }
    else{
      this.partner_design_order_data.design_data.items.forEach(element => {
        if (element.is_image == false && element.font != null){
          element.font = new Font(element.font);
        }
      });
    }
    console.log(this.partner_design_order_data);

  }

  toggleLayerMenu() {
    this.showLayerMenu ? this.hideLayerMenu() : this.showLayerMenu();
  }

  hideLayerMenu() {
    this.renderer.addClass(this.layer_menu.nativeElement, "animated");
    this.renderer.addClass(this.layer_menu.nativeElement, "slideOutLeft");
    this.renderer.addClass(this.layer_menu.nativeElement, "fast");
    setTimeout(() => {
      this.layer_menu_hidden = true;
      this.renderer.setStyle(
        this.layer_menu.nativeElement,
        "visibility",
        "hidden"
      );
      this.renderer.removeClass(this.layer_menu.nativeElement, "animated");
      this.renderer.removeClass(this.layer_menu.nativeElement, "slideOutLeft");
      this.renderer.removeClass(this.layer_menu.nativeElement, "fast");
    }, 200);
  }

  showLayerMenu() {
    this.renderer.addClass(this.layer_menu.nativeElement, "animated");
    this.renderer.addClass(this.layer_menu.nativeElement, "slideInLeft");
    this.renderer.addClass(this.layer_menu.nativeElement, "fast");
    this.renderer.setStyle(
      this.layer_menu.nativeElement,
      "visibility",
      "visible"
    );
    this.layer_menu_hidden = false;
    setTimeout(() => {
      this.renderer.removeClass(this.layer_menu.nativeElement, "animated");
      this.renderer.removeClass(this.layer_menu.nativeElement, "slideInLeft");
      this.renderer.removeClass(this.layer_menu.nativeElement, "fast");
    }, 200);
  }

  reorder(event) {
    console.log(event);
    // this.partner_design_order_data.design_data.items.forEach((img, i) => {
    //   img.index = i+1;
    // });
  }

  selectImage(img, index) {
    console.log(`selectImage img${JSON.stringify(img)}, index ${index}`);
    this.selected_image_id == img.id
      ? (this.selected_image_id = null)
      : (this.selected_image_id = img.id);

    this.selected_image_index == index
      ? (this.selected_image_index = null)
      : (this.selected_image_index = index);

    console.log(`selected_image_id: ${this.selected_image_id}`);

  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.partner_design_order_data.design_data.items,
      event.previousIndex,
      event.currentIndex
    );
    console.log("change index");

    //set img container index
    this.partner_design_order_data.design_data.items.forEach((item, i) => {
      item.z_index = (this.partner_design_order_data.design_data.items.length - i) + 1;
    });
    console.log(this.partner_design_order_data.design_data.items);
  }


  move(e) {
    //console.log(e)
  }

  end(event, index) {
    // console.log("Drag end");
    // console.log(event);

    this.saveState(JSON.parse(JSON.stringify(this.partner_design_order_data.design_data.items)));

    this.partner_design_order_data.design_data.items[index].position = {
      x: this.partner_design_order_data.design_data.items[index].position.x + event.distance.x,
      y: this.partner_design_order_data.design_data.items[index].position.y + event.distance.y
    };
  }

  released(e) {
    // console.log("Drag release");
    // console.log(e);
    //this.dragPosition = {x: e.distance.x, y: e.distance.y};
  }


  async presentActionSheet() {
    let actionSheet = await this.actionSheetCtrl.create({
      // title: 'Select Image Source',
      mode: "ios",
      cssClass: "actionSheetCss",
      buttons: [
        {
          text: ("從身機相簿選擇"),
          handler: () => {
            console.log("Load from Library");
            // this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: ("拍攝新照片"),
          handler: () => {
            console.log("Load Camera");
            // this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: ("取消"),
          role: "cancel"
        }
      ]
    });
    await actionSheet.present();
  }

  // takePicture(sourceType) {
  //   const options: CameraOptions = {
  //     quality: 50,
  //     //sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
  //     sourceType: sourceType,
  //     destinationType: this.camera.DestinationType.DATA_URL,
  //     encodingType: this.camera.EncodingType.JPEG,
  //     mediaType: this.camera.MediaType.PICTURE,
  //     correctOrientation: true
  //   };

  //   this.camera.getPicture(options).then(
  //     imageData => {
  //       this.partner_design_order_data.design_data.background = "data:image/jpeg;base64," + imageData;

  //       this.storage.set(
  //         DASHBOARD_ONE_BACKGROUND_DATA_KEY,
  //         this.partner_design_order_data.design_data.background
  //       );

  //       this.storage.remove(DASHBOARD_ONE_BACKGROUND_POSITION_KEY);
  //       this.background_width = 100;
  //       //this.background_height = null;
  //       this.background_x = 0;
  //       this.background_y = 50;

  //       //let image_data = { img_name: "image.jpg", img_data: ('data:image/jpeg;base64,' + imageData) };
  //       //let base64Image = 'data:image/jpeg;base64,' + imageData;
  //     },
  //     err => {
  //       console.log(err);
  //     }
  //   );
  // }

  resetDashboard() {
    this.partner_design_order_data.design_data.items = [];
    this.partner_design_order_data.design_data.background = null;
  }

  removeImage(index) {
    this.partner_design_order_data.design_data.items.splice(index, 1);
  }

  changeImage(image_name) {
    this.partner_design_order_data.design_data.items[this.selected_image_index].image = image_name;
  }

  pinchstart(e) {
    console.log(this.disabledDrag);
  }

  pinchend(e) {
    console.log("pinched");
    if (
      this.selected_image_index != undefined &&
      this.selected_image_index != null
    ) {

      this.saveState(JSON.parse(JSON.stringify(this.partner_design_order_data.design_data.items)));
      console.log(this.disabledDrag);
      //console.log(e.scale);

      console.log(this.partner_design_order_data.design_data.items[this.selected_image_index].width);
      console.log(
        this.partner_design_order_data.design_data.items[this.selected_image_index].width * e.scale
      );
      this.partner_design_order_data.design_data.items[this.selected_image_index].width =
        this.partner_design_order_data.design_data.items[this.selected_image_index].width * e.scale;
      this.partner_design_order_data.design_data.items[this.selected_image_index].height =
        this.partner_design_order_data.design_data.items[this.selected_image_index].height * e.scale;

    }
  }

  pinchout(e) {
    if (
      this.selected_image_index != undefined &&
      this.selected_image_index != null
    ) {
      console.log(this.disabledDrag);
      //console.log(e.scale);
      // this.saveState(JSON.parse(JSON.stringify(this.partner_design_order_data.design_data.items)));

      this.partner_design_order_data.design_data.items[this.selected_image_index].width = this.partner_design_order_data.design_data.items[this.selected_image_index].width * e.scale;
    }
  }

  pinchin(e) {
    if (
      this.selected_image_index != undefined &&
      this.selected_image_index != null
    ) {
      console.log(this.disabledDrag);
      //console.log(e.scale);
      //  this.saveState(JSON.parse(JSON.stringify(this.partner_design_order_data.design_data.items)));

      this.partner_design_order_data.design_data.items[this.selected_image_index].width =
        this.partner_design_order_data.design_data.items[this.selected_image_index].width * e.scale;

    }
  }

  rotate(index) {
    this.saveState(JSON.parse(JSON.stringify(this.partner_design_order_data.design_data.items)));
    this.partner_design_order_data.design_data.items[index].angle += 10;
  }

  horizontalFlip(index) {
    this.saveState(JSON.parse(JSON.stringify(this.partner_design_order_data.design_data.items)));

    if (this.partner_design_order_data.design_data.items[index].flip_x == 1) {
      this.partner_design_order_data.design_data.items[index].flip_x = -1;
    } else {
      this.partner_design_order_data.design_data.items[index].flip_x = 1;
    }
  }

  enlarge(index) {
    this.saveState(JSON.parse(JSON.stringify(this.partner_design_order_data.design_data.items)));

    this.partner_design_order_data.design_data.items[index].width = this.partner_design_order_data.design_data.items[index].width * 1.1;
    this.partner_design_order_data.design_data.items[index].height = this.partner_design_order_data.design_data.items[index].height * 1.1;

  }

  reduce(index) {
    this.saveState(JSON.parse(JSON.stringify(this.partner_design_order_data.design_data.items)));

    this.partner_design_order_data.design_data.items[index].width = this.partner_design_order_data.design_data.items[index].width * 0.9;
    this.partner_design_order_data.design_data.items[index].height = this.partner_design_order_data.design_data.items[index].height * 0.9;
  }

  verticalFlip(index) {
    this.saveState(JSON.parse(JSON.stringify(this.partner_design_order_data.design_data.items)));

    if (this.partner_design_order_data.design_data.items[index].flip_y == 1) {
      this.partner_design_order_data.design_data.items[index].flip_y = -1;
    } else {
      this.partner_design_order_data.design_data.items[index].flip_y = 1;
    }
  }

  undo() {
    this.restoreState(
      JSON.parse(JSON.stringify(this.partner_design_order_data.design_data.items)),
      this.undo_list,
      this.redo_list
    );
  }

  redo() {
    this.restoreState(
      JSON.parse(JSON.stringify(this.partner_design_order_data.design_data.items)),
      this.redo_list,
      this.undo_list
    );
  }

  restoreState(data, pop, push) {
    if (pop.length) {
      this.saveState(data, push, true);
      let restore_state = pop.pop();
      console.log(JSON.parse(JSON.stringify(restore_state)));
      this.partner_design_order_data.design_data.items = JSON.parse(JSON.stringify(restore_state));
    }
  }

  saveState(data, list?, keep_redo?) {
    keep_redo = keep_redo || false;
    if (!keep_redo) {
      this.redo_list = [];
    }

    (list || this.undo_list).push(data);
  }

  async resetDashboardAlert() {
    const alert = await this.alertController.create({
      header: "Are you sure to reset the dashboard?",
      mode: "ios",
      message: "",
      cssClass: "alertCss",
      buttons: [
        {
          text: "Yes",
          handler: () => {
            this.resetDashboard();
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Confirm Cancel: blah");
          }
        }
      ]
    });
    await alert.present();
  }

  getBackgroundStyle(image) {
    return `url(${image}) center center / contain no-repeat`;
  }

  getBackground() {
    if (this.partner_design_order_data.design_data.background != undefined && !this.partner_design_order_data.design_data.background.includes('dashboard_one_photo_')) {
      return 'url(' + this.partner_design_order_data.design_data.background + ')  no-repeat center center / contain fixed';
    }
    else if (this.partner_design_order_data.design_data.background != undefined && this.partner_design_order_data.design_data.background.includes('dashboard_one_photo_')) {
      return 'url(' + this.partner_design_order_data.design_data.background + ')  no-repeat center center / contain fixed';
    }
    else {
      return 'unset';
    }
  }

  preview() {
    this.htmlToImage().then(d => {
      if (d && this.partner_design_order_data.design_data.final_product_image){
        this.preview_mode = true;
      }
    });
    // (this.preview_mode ? this.preview_mode = false : this.preview_mode = true);
  }

  triggerImgUpload() {
    if (this.upload_img == null) {
      return;
    }
    this.upload_img.nativeElement.click();
  }
  uploadImg() {
    if (this.upload_img == null || this.util.isLoading) {
      return;
    }
    const fileList: FileList = this.upload_img.nativeElement.files;
    if (fileList && fileList.length > 0) {
      this.util.firstFileToBase64(fileList[0]).then(async (base64: string) => {
        console.log(base64);
        // this.user_data.profile_img = base64;
        let item = {
          "id": this.partner_design_order_data.design_data.items.length + 1,
          "is_image": true,
          "image": base64,
          "width": 150,
          "height": null,
          "position": { "x": 0, "y": 0 },
          "angle": 0,
          "flip_x": 1,
          "flip_y": 1,
          "z_index": this.partner_design_order_data.design_data.items.length + 1,
          "background_color": "transparent",
          "text": "",
          "text_color": "#000000",
          "font": null
        };
        this.partner_design_order_data.design_data.items.push(item);
      })

    }
  }

  htmlToImage(): Promise<any> {
    return new Promise((resolve, reject) => {
      var node = document.getElementById('canvas');

      htmlToImage.toPng(node)
        .then(dataUrl => {
          console.log(dataUrl);
          this.partner_design_order_data.design_data.final_product_image = dataUrl;
          resolve(true);
        })
        .catch(function (error) {
          console.error('oops, something went wrong!', error);
          resolve(false);
        });
    });
 
  }

  goQuotationPage(){
    if (this.partner_design_order_data.product_type == null){
      return;
    }
    this.util.presentLoading();
    this.htmlToImage().then(d => {
      console.log(d);
      this.util.dismissLoading();
      if (d){
        this.storageService.LOCAL_STORE.partner_design_order_data.next(this.partner_design_order_data);
        this.nav.navigateForward('quotation');
      }
    });
  }
  
  ionViewWillLeave(){
    this.storageService.LOCAL_STORE.partner_design_order_data.next(this.partner_design_order_data);

  }

  addNewTextItem(){
    let item = {
      "id": this.partner_design_order_data.design_data.items.length + 1,
      "is_image": false,
      "image": "",
      "width": 150,
      "height": null,
      "position": { "x": 0, "y": 0 },
      "angle": 0,
      "flip_x": 1,
      "flip_y": 1,
      "z_index": this.partner_design_order_data.design_data.items.length + 1,
      "background_color": "transparent",
      "text": "輸入文字",
      "text_color": "#000000",
      "font": null
    };
    this.partner_design_order_data.design_data.items.push(item);
  }

  fontPickerChange($event){
    console.log($event);
  }


  selectProductType($event){
    console.log($event);
    if ($event.detail.value == Design_Product_Type.tshirt && this.partner_design_order_data.product_type != Design_Product_Type.tshirt){
      this.partner_design_order_data.product_type = Design_Product_Type.tshirt;
      // this.partner_design_order_data.design_data.background = "../assets/icon/tshirt.svg";
    }
    else if ($event.detail.value == Design_Product_Type.phone_case && this.partner_design_order_data.product_type != Design_Product_Type.phone_case){
      this.partner_design_order_data.product_type = Design_Product_Type.phone_case;
      // this.partner_design_order_data.design_data.background = "../assets/icon/tshirt.svg";
    }
  }






}
