import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckoutCompletedPage } from './checkout-completed.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutCompletedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckoutCompletedPageRoutingModule {}
