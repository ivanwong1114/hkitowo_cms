import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckoutCompletedPageRoutingModule } from './checkout-completed-routing.module';

import { CheckoutCompletedPage } from './checkout-completed.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckoutCompletedPageRoutingModule
  ],
  declarations: [CheckoutCompletedPage]
})
export class CheckoutCompletedPageModule {}
