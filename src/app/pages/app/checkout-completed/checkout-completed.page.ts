import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkout-completed',
  templateUrl: './checkout-completed.page.html',
  styleUrls: ['./checkout-completed.page.scss'],
})
export class CheckoutCompletedPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
