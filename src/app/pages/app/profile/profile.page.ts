import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { data_type } from 'src/app/interface';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';
import { UtilService } from 'src/app/services/util.service';
import { environment } from 'src/environments/environment';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  edit_profile: boolean = false;
  new_password: string = null;

  user_data = JSON.parse(JSON.stringify(this.auth.userData.value));
  @ViewChild('upload_img', { static: false }) upload_img: ElementRef;

  isDebug = !environment.production;

  public get DataType(): typeof data_type {
    return data_type;
  }
  constructor(
    public auth: AuthService,
    private storageService: StorageService,
    private util: UtilService,
    private alertController: AlertController,
    private iab: InAppBrowser
  ) { }

  ngOnInit() {
  }

  resetUserData(){
    this.edit_profile = false;
    this.user_data = this.auth.userData.value;
  }

  updateProfile(){
    if (this.new_password != null && this.new_password != ''){
      this.user_data.password = this.new_password;
    }
    this.storageService.saveToDatabase('edit', this.user_data.data_type, this.user_data);
    this.auth.updateUserData(this.user_data);
    this.edit_profile = false;
    this.util.presentToast('已更新個人檔案');
  }

  triggerImgUpload() {
    if (this.upload_img == null) {
      return;
    }
    this.upload_img.nativeElement.click();
  }
  uploadImg() {
    if (this.upload_img == null || this.util.isLoading) {
      return;
    }
    const fileList: FileList = this.upload_img.nativeElement.files;
    if (fileList && fileList.length > 0) {
      this.util.firstFileToBase64(fileList[0]).then(async (base64: string) => {
        console.log(base64);
        this.user_data.profile_img = base64;
      })
    }
  }

  async presentLogoutAlert() {
    const alert = await this.alertController.create({
      // cssClass: 'my-custom-class',
      header: '確認登出？',
      // message: '確認取消預約？',
      buttons: [
        {
          text: '否',
          role: 'cancel',
          cssClass: 'secondary',
          id: 'cancel-button',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '是',
          id: 'confirm-button',
          handler: () => {
            this.auth.Logout();
          }
        }
      ]
    });

    await alert.present();
  }

  async presentDeleteAccountAlert() {
    const alert = await this.alertController.create({
      // cssClass: 'my-custom-class',
      header: '確應刪除帳號？',
      // message: '確認取消預約？',
      buttons: [
        {
          text: '是',
          id: 'confirm-button',
          handler: () => {
            this.auth.deleteAccount();
          }
        },
        {
          text: '否',
          role: 'cancel',
          cssClass: 'secondary',
          id: 'cancel-button',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }
      ]
    });

    await alert.present();
  }

  privacyPolicy(){
    this.iab.create(`https://hkitowo.netlify.app/privacy`, '_system');
  }

  terms(){
    this.iab.create(`https://hkitowo.netlify.app/terms`, '_system');
  }

}
