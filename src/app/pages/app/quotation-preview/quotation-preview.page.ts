import { Component, ElementRef, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { Activity, Activity_Title, data_type, System } from 'src/app/interface';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';
import { UtilService } from 'src/app/services/util.service';
import { PDFGenerator } from '@awesome-cordova-plugins/pdf-generator/ngx';


@Component({
  selector: 'app-quotation-preview',
  templateUrl: './quotation-preview.page.html',
  styleUrls: ['./quotation-preview.page.scss'],
})
export class QuotationPreviewPage implements OnInit {

  partner_design_order_data = this.storageService.LOCAL_STORE.partner_design_order_data.value;
  system_data: System = this.storageService.LOCAL_STORE.system_data.value;

  constructor(
    private storageService: StorageService,
    private auth: AuthService,
    private elementRef: ElementRef,
    private nav: NavController,
    public util: UtilService,
    private alertController: AlertController,
    private pdfGenerator: PDFGenerator
  ) { }

  ngOnInit() {
    console.log(this.partner_design_order_data);
  }

  next(){
    this.partner_design_order_data.quotation_html = document.getElementById('quotation').innerHTML;
    this.presentConfirmAlert();
  }

  async presentConfirmAlert() {
    const alert = await this.alertController.create({
      // cssClass: 'my-custom-class',
      header: '確認訂單？',
      message: `此訂單佣金為$${this.partner_design_order_data.commission}`,
      buttons: [
        {
          text: '否',
          role: 'cancel',
          cssClass: 'secondary',
          id: 'cancel-button',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '是',
          id: 'confirm-button',
          handler: () => {
            console.log(this.partner_design_order_data);
            this.storageService.saveToDatabase('create', this.partner_design_order_data.data_type, this.partner_design_order_data);

            let activity_data: Activity = this.storageService.createInitData(data_type.activity_data);
            activity_data.title = Activity_Title.create_new_design_order;
            activity_data.partner_id = this.auth.userData.value.id;
            this.storageService.saveToDatabase('create', data_type.activity_data, activity_data);

            this.nav.navigateRoot('');
            this.util.presentToast('已建立訂單');
            setTimeout(() => {
              this.storageService.LOCAL_STORE.partner_design_order_data.next(null);
            }, 500);
          }
        }
      ]
    });

    await alert.present();
  }

  getPDF() {
 
    let htmlSample = document.getElementById('quotation').innerHTML;
    let options = {
      documentSize: 'A4',
      type: 'share',
      fileName: `quotation_${this.util.addLeadingZeros((this.partner_design_order_data.id), 6)}.pdf`
    }
 
    this.pdfGenerator.fromData(htmlSample, options).
      then(resolve => {
        console.log(resolve);
 
      }
      ).catch((err) => {
        console.error(err);
      });
  }
  
}
