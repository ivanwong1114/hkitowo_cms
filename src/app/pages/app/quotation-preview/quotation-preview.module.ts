import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QuotationPreviewPageRoutingModule } from './quotation-preview-routing.module';

import { QuotationPreviewPage } from './quotation-preview.page';
import { SharedModule } from 'src/app/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QuotationPreviewPageRoutingModule,
    SharedModule
  ],
  declarations: [QuotationPreviewPage]
})
export class QuotationPreviewPageModule {}
