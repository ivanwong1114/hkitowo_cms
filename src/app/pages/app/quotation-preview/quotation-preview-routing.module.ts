import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuotationPreviewPage } from './quotation-preview.page';

const routes: Routes = [
  {
    path: '',
    component: QuotationPreviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuotationPreviewPageRoutingModule {}
