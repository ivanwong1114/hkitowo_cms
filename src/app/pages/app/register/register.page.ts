import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Customer, data_type, General_Status } from 'src/app/interface';
import { StorageService } from 'src/app/services/storage.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  // new_customer: Customer = {
  //   id: null,
  //   data_type: data_type.customer_data,
  //   create_datetime: new Date(),
  //   status: General_Status.created,
  //   username: "",
  //   email: "",
  //   phone: "",
  //   display_name: "",
  //   month_of_birth: "",
  //   password: "",
  //   is_vip: false,
  //   profile_img: ""
  // }

  new_customer: Customer = null;

  confirm_password = "";

  constructor(
    public util: UtilService,
    public storageService: StorageService,
    private nav: NavController
  ) { }

  ngOnInit() {
    this.new_customer = this.storageService.createInitData(data_type.customer_data);
    console.log(this.new_customer);
  }

  register(){
    if (this.new_customer.display_name == ''){
      return this.util.presentToast("請輸入名稱");
    }
    if (this.new_customer.email == ''){
      return this.util.presentToast("請輸入電郵地址");
    }
    if (this.new_customer.phone == ''){
      return this.util.presentToast("請輸入電話");
    }
    if (this.new_customer.password == ''){
      return this.util.presentToast("請輸入密碼");
    }
    if (this.confirm_password == ''){
      return this.util.presentToast("請重覆輸入密碼");
    }
    // if (this.new_customer.month_of_birth == ''){
    //   return this.util.presentToast("請選擇出生月份");
    // }
    if (this.new_customer.password != this.confirm_password){
      return this.util.presentToast("密碼不一致");
    }
    if (this.new_customer.referrer_code != ''){
      let index = this.storageService.LOCAL_STORE.customer_data_list.value.findIndex((c: Customer) => c.referral_code == this.new_customer.referrer_code);
      if (index == -1){
        return this.util.presentToast("推薦碼不正確");
      }
    }
    this.storageService.saveToDatabase('create', data_type.customer_data, this.new_customer);
    if (this.new_customer.referrer_code != ''){
      this.storageService.assignCreditToCustomer('50', this.new_customer.id);
    }
    this.nav.pop();
    this.util.presentToast("成功註冊!");
  }

}
