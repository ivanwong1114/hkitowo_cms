import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/guard/auth.guard';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../home/home.module').then( m => m.HomePageModule),
        // canActivate: [AuthGuard]
      },
      {
        path: 'design',
        loadChildren: () => import('../design/design.module').then( m => m.DesignPageModule),
        // canActivate: [AuthGuard]
      },
      {
        path: 'profile',
        loadChildren: () => import('../profile/profile.module').then( m => m.ProfilePageModule),
        // canActivate: [AuthGuard]
      },
      {
        path: 'appointment',
        loadChildren: () => import('../appointment/appointment.module').then( m => m.AppointmentPageModule),
        // canActivate: [AuthGuard]
      },
      {
        path: 'shopping',
        loadChildren: () => import('../shopping/shopping.module').then( m => m.ShoppingPageModule),
        // canActivate: [AuthGuard]
      },
      {
        path: 'order',
        loadChildren: () => import('../order/order.module').then( m => m.OrderPageModule),
        // canActivate: [AuthGuard]
      },
      {
        path: 'redeem',
        loadChildren: () => import('../redeem/redeem.module').then( m => m.RedeemPageModule),
        // canActivate: [AuthGuard]
      },
      {
        path: 'report',
        loadChildren: () => import('../report/report.module').then( m => m.ReportPageModule),
        // canActivate: [AuthGuard]
      },
      {
        path: 'partner-appointment',
        loadChildren: () => import('../partner-appointment/partner-appointment.module').then( m => m.PartnerAppointmentPageModule),
        // canActivate: [AuthGuard]
      },
      {
        path: '',
        redirectTo: '/tabs/shopping',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/shopping',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
