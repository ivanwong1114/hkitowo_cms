import { Component } from '@angular/core';
import { data_type } from 'src/app/interface';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  public get dataType(): typeof data_type {
    return data_type;
  }

  constructor(
    public auth: AuthService
  ) {}

}
