import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import { NavigationOptions } from '@ionic/angular/providers/nav-controller';
import { Order, Order_Status } from 'src/app/interface';
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/services/data.service';
import { StorageService } from 'src/app/services/storage.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {

  my_orders: any[] = null;

  public get OrderStatus(): typeof Order_Status {
    return Order_Status;
  }
  constructor(
    private storageService: StorageService,
    private dataService: DataService,
    private alertController: AlertController,
    private util: UtilService,
    private modalController: ModalController,
    public auth: AuthService,
    private nav: NavController
  ) { }

  ngOnInit() {
    if (this.auth.userData.value != null){
      this.my_orders = this.dataService.getCustomerOrder();
      if (this.my_orders){
        this.my_orders.map((o: Order) => {
          let init = 0;
          o['total_quantity'] = o.cart.map(d => d.quantity).reduce( (previousValue, currentValue) => previousValue + currentValue, init);
          return
        });
      }
  
      console.log(this.my_orders);
    }

  }

  goOrderDetail(order: Order){
    let options: NavigationOptions = {
      //   replaceUrl: options?.replaceUrl,
      //   skipLocationChange: options?.skipLocationChange,
        state: order
    }
    this.nav.navigateForward('order-detail', options);
  }


  async presentCancelAlert(order: Order) {
    const alert = await this.alertController.create({
      // cssClass: 'my-custom-class',
      header: '確認取消訂單？',
      // message: '確認取消預約？',
      buttons: [
        {
          text: '否',
          role: 'cancel',
          cssClass: 'secondary',
          id: 'cancel-button',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: '是',
          id: 'confirm-button',
          handler: () => {
            this.cancelAppointment(order);
          }
        }
      ]
    });

    await alert.present();
  }


  cancelAppointment(order: Order) {
    order.status = Order_Status.cancelled;
    this.storageService.saveToDatabase('edit', order.data_type, order);
    this.util.presentToast("已取消訂單");
  }

}
