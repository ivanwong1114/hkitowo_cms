import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';
import SignaturePad from 'signature_pad';
import { IonContent, NavController } from '@ionic/angular';
import { UtilService } from 'src/app/services/util.service';
import { environment } from 'src/environments/environment';
import { Design_Product, System } from 'src/app/interface';

@Component({
  selector: 'app-quotation',
  templateUrl: './quotation.page.html',
  styleUrls: ['./quotation.page.scss'],
})
export class QuotationPage implements OnInit {

  user_data = this.auth.userData.value;

  partner_design_order_data = this.storageService.LOCAL_STORE.partner_design_order_data.value;
  design_product_list = this.storageService.LOCAL_STORE.system_data.value.design_product_list.filter((d: Design_Product) => d.product_type == this.partner_design_order_data.product_type);
  system_data: System = this.storageService.LOCAL_STORE.system_data.value;

  @ViewChild(IonContent, { static: false }) ion_content: IonContent;
  constructor(
    private storageService: StorageService,
    private auth: AuthService,
    private elementRef: ElementRef,
    private nav: NavController,
    private util: UtilService
  ) { }

  ngOnInit(): void {
    if (!environment.production){
      this.partner_design_order_data.customer_name = 'testing name';
      this.partner_design_order_data.customer_email = 'testing@gmail.com';
      this.partner_design_order_data.customer_phone = '61342344';
    }
  }


  next() {
    if (this.partner_design_order_data.customer_name == null || this.partner_design_order_data.customer_name == ''){
      return this.util.presentToast("請輸入顧客名稱");
    }
    if (this.partner_design_order_data.customer_email == null || this.partner_design_order_data.customer_email == ''){
      return this.util.presentToast("請輸入顧客電郵");
    }
    if (this.partner_design_order_data.customer_phone == null || this.partner_design_order_data.customer_phone == ''){
      return this.util.presentToast("請輸入顧客電話");
    }
    if (this.partner_design_order_data.quantity == null || this.partner_design_order_data.quantity <= 0){
      return this.util.presentToast("請輸入正確的數量");
    }
    if (this.partner_design_order_data.design_product == null){
      return this.util.presentToast("請選擇產品款式");
    }

    if (this.partner_design_order_data.delivery_data.delivery_method == 'sf_delivery'){
      if (this.partner_design_order_data.delivery_data.delivery_address == ''){
        return this.util.presentToast("請輸入送貨地址");
      }
    }
    this.storageService.LOCAL_STORE.partner_design_order_data.next(this.partner_design_order_data);
    console.log(this.partner_design_order_data);
    setTimeout(() => {
      this.nav.navigateForward('signature');
    }, 100);
  }

  deliveryMethodChange($event){
    // console.log($event);
    // if ($event.detail.value == 'store_pickup' || $event.detail.value == 'sf_delivery'){
    //   this.customer_order_data.delivery_data.delivery_method = $event.detail.value;
    // }
    setTimeout(() => {
      this.ion_content.scrollToBottom(300);
    }, 100);
  }

  selectDesignProduct($event){
    console.log($event);
    let design_product: Design_Product = $event.detail.value;
    this.partner_design_order_data.subtotal = (+design_product.price)*this.partner_design_order_data.quantity;
    this.partner_design_order_data.total_amount = this.partner_design_order_data.subtotal;
    this.partner_design_order_data.commission = this.partner_design_order_data.total_amount*this.system_data.commission_rate_per_desgin_order/100;
    console.log(this.partner_design_order_data);
  }


}
