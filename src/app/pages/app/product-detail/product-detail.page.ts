import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonContent, IonSlides } from '@ionic/angular';
import { data_type, Order, Product } from 'src/app/interface';
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/services/data.service';
import { OrderService } from 'src/app/services/order.service';
import { StateService } from 'src/app/services/state.service';
import { StorageService } from 'src/app/services/storage.service';
import { UtilService } from 'src/app/services/util.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {
  @ViewChild(IonContent, { static: false }) content: IonContent;
  @ViewChild('slides', { static: false }) slides: IonSlides;
  product: any = null;
  slideOpts = {
    autoHeight: true,
  };
  ion_content_height = window.innerHeight;

  cart_counter = 1;

  user_data = this.authService.userData.value;

  is_redeem: boolean;

  constructor(
    private state: StateService,
    private storageService: StorageService,
    private authService: AuthService,
    private dataService: DataService,
    public orderService: OrderService,
    private router: Router,
    private utilService: UtilService
  ) { 
    if (router.getCurrentNavigation().extras.state) {
      let state: any = this.router.getCurrentNavigation().extras.state;
      this.is_redeem = state.is_redeem;
    }
  }

  ngOnInit() {
    this.product = this.state.current_product$.value;
    if (this.product == null && environment.production == false) {
      this.product = this.storageService.LOCAL_STORE.product_data_list.value[0];
    }
    this.product['attributes_display'] = this.product.attributes.join('/ ');
    console.log(this.product);
    
    this.orderService.checkCustomerOrderData();
  }

  async ionViewDidEnter() {
    let el = await this.content.getScrollElement();
    this.ion_content_height = el.offsetHeight;
  }

  getMinHeight(slides_container_height) {
    return (this.ion_content_height - slides_container_height) + 'px';
  }


  async ionSlidesDidLoad($event) {
    this.slides.update();
  }

  async ionSlideDidChange($event) {
    this.slides.updateAutoHeight();
  }

  addOrRemoveCartCounter(type: 'add' | 'remove') {
    if (type == 'add') {
      this.cart_counter++;
    }
    else if (type == 'remove') {
      this.cart_counter--;
    }
  }

}
