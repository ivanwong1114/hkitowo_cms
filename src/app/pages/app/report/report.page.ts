import { Component, OnInit } from '@angular/core';
import { Design_Order } from 'src/app/interface';
import { DataService } from 'src/app/services/data.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {
  segment_value: 'day' | 'week' | 'month' = 'day'

  selected_date = null;

  data = null;

  design_order_data_list: Design_Order[] = null;

  today = new Date().toISOString();

  constructor(
    private storageService: StorageService,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.design_order_data_list = this.dataService.getPartnerDesignOrder();
    console.log(this.design_order_data_list);
  }

  ionViewDidEnter(){
  }

  dateChange($event) {
    // console.log($event);
    // console.log(this.selected_date);


    this.getReportData();
    // console.log(this.getDatesInRange(one_month_before, new Date(this.selected_date.split('T')[0])));
    // console.log(this.getDatesInRange(one_month_before, new Date(this.selected_date.split('T')[0])).length);
  }

  getReportData(){
    switch (this.segment_value) {
      case 'day':
        let selected_date_string = this.selected_date.split('T')[0];
        const one_month_before = this.subtractMonths(1, new Date(selected_date_string));
        this.data = [];
        this.getDatesInRange(one_month_before, new Date(selected_date_string)).reverse().forEach(date => {
          let date_string = new Date(date).toISOString().split('T')[0];
          let a = this.design_order_data_list.filter((d: Design_Order) => new Date(d.create_datetime).toISOString().split('T')[0] == date_string);
          this.data.push({
            'date': date_string,
            "sales": a != null && a.length > 0 ? a.map(d => d.total_amount).reduce(function (a, b) { return a + b; }) : 0,
            'commission': a != null && a.length > 0 ? a.map(d => d.commission).reduce(function (a, b) { return a + b; }) : 0,
            'quantity': a.length
          })
        });
        break;
    
      default:
        break;
    }
    console.log(this.data);
  }

  segmentChanged($event) {
    // console.log($event);
    this.segment_value = $event.detail.value;

  }

  getDatesInRange(startDate, endDate) {
    const date = new Date(startDate.getTime());

    const dates = [];

    while (date <= endDate) {
      dates.push(new Date(date));
      date.setDate(date.getDate() + 1);
    }

    return dates;
  }

  subtractMonths(numOfMonths, date = new Date()) {
    date.setMonth(date.getMonth() - numOfMonths);

    return date;
  }



}
