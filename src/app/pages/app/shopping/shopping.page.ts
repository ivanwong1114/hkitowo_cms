import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Banner, data_type, General_Status, Product } from 'src/app/interface';
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/services/data.service';
import { StateService } from 'src/app/services/state.service';
import { StorageService } from 'src/app/services/storage.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-shopping',
  templateUrl: './shopping.page.html',
  styleUrls: ['./shopping.page.scss'],
})
export class ShoppingPage implements OnInit {

  banner_data_list: Banner[] = null;
  filterValue = '';

  constructor(
    private storageService: StorageService,
    private dataService: DataService,
    private util: UtilService,
    private nav: NavController,
    private state: StateService,
    public auth: AuthService
  ) { }

  ngOnInit() {


    // setTimeout(() => {
    //   this.storageService.LOCAL_STORE.customer_order_data.next(null);
    // }, 2000);
  }

  ionViewDidEnter(){
    if (this.auth.userData.value != null && this.auth.userData.value?.data_type != data_type.customer_data){
      return this.nav.navigateRoot('/tabs/design');
    }
    setTimeout(() => {
      this.banner_data_list = this.storageService.LOCAL_STORE.banner_data_list.value ? this.storageService.LOCAL_STORE.banner_data_list.value.filter((b: Banner) => b.status != General_Status.deleted && b.status != General_Status.inactive) : this.storageService.LOCAL_STORE.banner_data_list.value;
    }, 1000);
    console.log(this.banner_data_list);
  }

  goShoppingBag(){
    this.nav.navigateForward('shopping-bag');
  }

  goLogin(){
    this.nav.navigateRoot('login');
  }

}
