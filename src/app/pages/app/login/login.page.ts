import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Activity, Activity_Title, data_type } from 'src/app/interface';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  data_type: data_type.customer_data | data_type.partner_data = data_type.customer_data;
  email: string;
  password: string;

  public get dataType(): typeof data_type {
    return data_type;
  }

  constructor(
    private utilService: UtilService,
    private authService: AuthService,
    private nav: NavController,
    private storageService: StorageService
  ) { }

  ngOnInit() {
    setTimeout(() => {
      console.log(this.storageService.LOCAL_STORE.customer_data_list.value);
      console.log(this.storageService.LOCAL_STORE.partner_data_list.value);
    }, 1000);
  }

  changeLoginType(){
    if (this.data_type == data_type.customer_data){
      this.data_type = data_type.partner_data;
    }
    else{
      this.data_type = data_type.customer_data;
    }
  }

  login(){
      if (this.email == null || this.email == ''){
      this.utilService.openSnackBar("請輸入電郵");
      return;
    }
    if (this.password == null || this.password == ''){
      this.utilService.openSnackBar("請輸入密碼");
      return;
    }
    if (this.utilService.isLoading){
      return;
    }
    this.utilService.isLoading = true;
    this.authService.AppLogin(this.data_type ,this.email, this.password).then( res => {
      this.utilService.isLoading = false;
      console.log(res);
      if (res != null){
        let activity_data: Activity = this.storageService.createInitData(data_type.activity_data);
        activity_data.title = Activity_Title.login;
        activity_data.partner_id = this.authService.userData.value.id;
        this.storageService.saveToDatabase('create', data_type.activity_data, activity_data);
        
        this.nav.navigateRoot('');
      }
      // else{
      //   // this.verify_code_field.setFocus();
      // }
    });
  }

  goShopping(){
    this.nav.navigateRoot('');
  }

}
