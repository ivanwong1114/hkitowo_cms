import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';
import SignaturePad from 'signature_pad';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-signature',
  templateUrl: './signature.page.html',
  styleUrls: ['./signature.page.scss'],
})
export class SignaturePage implements OnInit {

  @ViewChild('canvas', { static: true }) signaturePadElement;
  signaturePad: any;
  canvasWidth: number;
  canvasHeight: number;

  partner_design_order_data = this.storageService.LOCAL_STORE.partner_design_order_data.value;

  constructor(
    private storageService: StorageService,
    private auth: AuthService,
    private elementRef: ElementRef,
    private nav: NavController
  ) { }

  ngOnInit() {
    this.init();
  }

  public ngAfterViewInit(): void {
    this.signaturePad = new SignaturePad(this.signaturePadElement.nativeElement);
    this.signaturePad.clear();
    this.signaturePad.penColor = 'rgb(104,214,165)';
  }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.init();
  }

  init() {
    const canvas: any = this.elementRef.nativeElement.querySelector('canvas');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight - 140;
    if (this.signaturePad) {
      this.signaturePad.clear(); // Clear the pad on init
    }
  }


  prev() {
    this.nav.pop();
  }


  next() {
    const img = this.signaturePad.toDataURL();
    // this.nav.navigateForward('quotation');
    this.partner_design_order_data.signature_image = img;
    this.storageService.LOCAL_STORE.partner_design_order_data.next(this.partner_design_order_data);
    this.nav.navigateForward('quotation-preview');
  }


  save(): void {
    const img = this.signaturePad.toDataURL();
  }

  isCanvasBlank(): boolean {
    if (this.signaturePad) {
      return this.signaturePad.isEmpty() ? true : false;
    }
  }

  clear() {
    this.signaturePad.clear();
  }

  undo() {
    const data = this.signaturePad.toData();
    if (data) {
      data.pop(); // remove the last dot or line
      this.signaturePad.fromData(data);
    }
  }


}
