import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DesignOrderPageRoutingModule } from './design-order-routing.module';

import { DesignOrderPage } from './design-order.page';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/app/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DesignOrderPageRoutingModule,
    MaterialModule,
    SharedModule
  ],
  declarations: [DesignOrderPage]
})
export class DesignOrderPageModule {}
