import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DesignOrderPage } from './design-order.page';

const routes: Routes = [
  {
    path: '',
    component: DesignOrderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DesignOrderPageRoutingModule {}
