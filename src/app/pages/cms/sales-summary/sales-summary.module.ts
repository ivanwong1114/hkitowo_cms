import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SalesSummaryPageRoutingModule } from './sales-summary-routing.module';

import { SalesSummaryPage } from './sales-summary.page';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/app/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SalesSummaryPageRoutingModule,
    MaterialModule,
    SharedModule
  ],
  declarations: [SalesSummaryPage]
})
export class SalesSummaryPageModule {}
