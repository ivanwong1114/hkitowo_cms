import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Partner } from 'src/app/interface';
import { CsvService } from 'src/app/services/csv.service';
import { DataService } from 'src/app/services/data.service';
import { StateService } from 'src/app/services/state.service';
import { StorageService } from 'src/app/services/storage.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-sales-summary',
  templateUrl: './sales-summary.page.html',
  styleUrls: ['./sales-summary.page.scss'],
})
export class SalesSummaryPage implements OnInit {

  partner_data: Partner = null;

  displayedColumns: string[] = ['status', 'design_product', "quantity", "total_amount", 'commission', "create_datetime"];
  dataSource = new MatTableDataSource(null);

  @ViewChild(MatTable, { static: false }) table: MatTable<any>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private state: StateService,
    private storageService: StorageService,
    private cdr: ChangeDetectorRef,
    private dataService: DataService,
    private csvService: CsvService
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.partner_data = this.state.sales_summary_partner.value;


    }, 100);

    setTimeout(() => {
      if (environment.production == false) {
        // console.log(this.storageService.LOCAL_STORE.partner_data_list.value);
        this.partner_data = this.storageService.LOCAL_STORE.partner_data_list.value[0];
        console.log(this.partner_data);
      }


      console.log(this.dataService.getPartnerDesignOrder(this.partner_data.id));
      this.initTable(this.dataService.getPartnerDesignOrder(this.partner_data.id));
    }, 500);
  }

  initTable(data) {
    this.dataSource = new MatTableDataSource(data);
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.cdr.detectChanges();
    }, 300);

  }


  download() {
    //  ['create_datetime','status', 'customer_phone', 'customer_name', 'customer_email', "product_type", "product_detail", "quantity", "total_amount", "commission"]);
    let data_to_export = [];
    this.dataService.getPartnerDesignOrder(this.partner_data.id).forEach(data => {
      data_to_export.push(
        {
          "create_datetime": data.create_datetime,
          "status": data.status,
          "customer_phone": data.customer_phone,
          "customer_name": data.customer_name,
          "customer_email": data.customer_email,
          "product_type": data.product_type == 'tshirt' ? 'T-shirt' : 'Phone case',
          "product_attribute": data.design_product.attribute,
          "product_detail": data.product_detail,
          "quantity": data.quantity,
          "total_amount": data.total_amount,
          "commission": data.commission
        }
      );
    });
    this.csvService.downloadFile(data_to_export, 'design-order-sales-summary', `report_${new Date().getTime()}`);
  }


}
