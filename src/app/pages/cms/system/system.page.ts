import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-system',
  templateUrl: './system.page.html',
  styleUrls: ['./system.page.scss'],
})
export class SystemPage implements OnInit {

  system_data = null;

  constructor(
    private storageService: StorageService
  ) { }

  ngOnInit() {
    this.system_data = JSON.parse(JSON.stringify(this.storageService.LOCAL_STORE.system_data.value));
    console.log(this.system_data);
  }

  saveSystemData(field){
    let system_data = this.storageService.LOCAL_STORE.system_data.value;
    system_data[field] = Number(this.system_data[field]);
    this.storageService.LOCAL_STORE.system_data.next(system_data);
  }

}
