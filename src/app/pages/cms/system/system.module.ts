import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SystemPageRoutingModule } from './system-routing.module';

import { SystemPage } from './system.page';
import { SharedModule } from 'src/app/shared.module';
import { MaterialModule } from 'src/app/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SystemPageRoutingModule,
    SharedModule,
    MaterialModule
  ],
  declarations: [SystemPage]
})
export class SystemPageModule {}
