import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CalendarMode, Step } from 'ionic2-calendar/calendar';
import { Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import { Appointment, Customer, Partner, Reservation_Timeslot } from 'src/app/interface';
import { ModalService } from 'src/app/services/modal.service';
import { StorageService } from 'src/app/services/storage.service';
import { CalendarComponent } from "ionic2-calendar";

@Component({
    selector: 'app-reservation',
    templateUrl: './reservation.page.html',
    styleUrls: ['./reservation.page.scss'],
})
export class ReservationPage implements OnInit {
    @ViewChild(CalendarComponent, {static: false}) myCalendar:CalendarComponent;

    eventSource;
    viewTitle;

    isToday: boolean;
    calendar = {
        mode: 'month' as CalendarMode,
        step: 30 as Step,
        currentDate: new Date(),
        dateFormatter: {
            formatMonthViewDay: function (date: Date) {
                return date.getDate().toString();
            },
            formatMonthViewDayHeader: function (date: Date) {
                return 'MonMH';
            },
            formatMonthViewTitle: function (date: Date) {
                return 'testMT';
            },
            formatWeekViewDayHeader: function (date: Date) {
                return 'MonWH';
            },
            formatWeekViewTitle: function (date: Date) {
                return 'testWT';
            },
            formatWeekViewHourColumn: function (date: Date) {
                return 'testWH';
            },
            formatDayViewHourColumn: function (date: Date) {
                return 'testDH';
            },
            formatDayViewTitle: function (date: Date) {
                return 'testDT';
            }
        }
    };

    reservationSub$$: Subscription = null;
    appointmentSub$$: Subscription = null;

    constructor(
        private navController: NavController,
        public modalService: ModalService,
        private storageService: StorageService
    ) {

    }

    ngOnInit() {
        this.reservationSub$$ = this.storageService.LOCAL_STORE.reservation_timeslot_data_list
            .pipe(distinctUntilChanged())
            .subscribe(() => {
                this.loadReservationAndAppointment();
            });
        this.appointmentSub$$ = this.storageService.LOCAL_STORE.appointment_data_list
            .pipe(distinctUntilChanged())
            .subscribe(() => {
                this.loadReservationAndAppointment();
            });
    }

    ngOnDestory(){
        this.reservationSub$$.unsubscribe();
        this.appointmentSub$$.unsubscribe();
    }

    ionViewDidEnter() {
        // this.modalService?.presentTableModal('create', null, 'reservation_timeslot_data');

        // setTimeout(() => {
        // this.loadReservationAndAppointment();
        // }, 2000);

    }

    loadEvents() {
        this.eventSource = this.createRandomEvents();
    }

    onViewTitleChanged(title) {
        this.viewTitle = title;
    }

    onEventSelected(event) {
        console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
    }

    changeMode(mode) {
        this.calendar.mode = mode;
    }

    today() {
        this.calendar.currentDate = new Date();
    }

    onTimeSelected(ev) {
        console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
            (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
    }

    slide(type:'next'|'previous'){
        if (type == 'next'){
            this.myCalendar.slideNext();
        }
        else{
            this.myCalendar.slidePrev();
        }
    }

    onCurrentDateChanged(event: Date) {
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        event.setHours(0, 0, 0, 0);
        this.isToday = today.getTime() === event.getTime();
    }

    loadReservationAndAppointment() {

        let reservation_timeslot_data_list: Reservation_Timeslot[] = this.storageService.LOCAL_STORE.reservation_timeslot_data_list.value;
        let appointment_data_list: Appointment[] = this.storageService.LOCAL_STORE.appointment_data_list.value;
        let customer_data_list: Customer[] = this.storageService.LOCAL_STORE.customer_data_list.value;
        let partner_data_list: Partner[] = this.storageService.LOCAL_STORE.partner_data_list.value;
        // console.log(reservation_timeslot_data_list);
        // console.log(appointment_data_list);
        if (reservation_timeslot_data_list && appointment_data_list) {
            reservation_timeslot_data_list.forEach((r: Reservation_Timeslot) => {
                r['appointments'] =
                    appointment_data_list
                        .filter((a: Appointment) => a.reservation_timeslot_id == r.id)
                        .map(a => {
                            a['customer'] = customer_data_list.find(d => d.id == a.customer_id);
                            a['partner'] = partner_data_list.find(d => d.id == a.partner_id);
                            return a;
                        });
            });
            this.addEvent(reservation_timeslot_data_list);
        }

    }

    addEvent(data_list: any[]) {
        var events = [];
        data_list.forEach((data: Reservation_Timeslot) => {
            let dt = new Date(data.date);
            dt.setHours(0, 0, 0, 0);
            dt.setMinutes(dt.getMinutes() + dt.getTimezoneOffset());
            let dt2 = new Date(data.date);
            dt2.setHours(0, 0, 0, 0);
            dt2.setMinutes(dt2.getMinutes() + dt2.getTimezoneOffset());

            let startTimeSecond = (+data.start_time.split(':')[0]) * 60 * 60 + (+data.start_time.split(':')[1]) * 60 + (+"00");
            let startTime = dt.setSeconds(dt.getSeconds() + startTimeSecond + (8 * 60 * 60));
            let endTimeSecond = (+data.end_time.split(':')[0]) * 60 * 60 + (+data.end_time.split(':')[1]) * 60 + (+"00");
            let endTime = dt2.setSeconds(dt2.getSeconds() + endTimeSecond + (8 * 60 * 60));
            // console.log(data.date);
            // console.log(data.start_time);
            // console.log(startTime);
            // console.log(new Date(startTime));

            // console.log(data['appointments']);
            data['appointments'].forEach((appointment: Appointment) => {
                events.push({
                    title: `Quota: ${data.quota}, Price: $${data.price}, Customer: ${appointment['customer'].display_name}, Partner: ${appointment['partner'].display_name}`,
                    startTime: new Date(startTime),
                    endTime: new Date(endTime),
                    allDay: false
                });
            });
            this.eventSource = events;
        });

        console.log(events);
        console.log(this.eventSource);
    }

    createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }
        }
        console.log(events);
        return events;
    }

    onRangeChanged(ev) {
        console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
    }

    markDisabled = (date: Date) => {
        var current = new Date();
        current.setHours(0, 0, 0);
        return date < current;
    };

}
