import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { NavController } from '@ionic/angular';
import { UtilService } from 'src/app/services/util.service';
import { AuthService } from 'src/app/services/auth.service';
import { data_type } from 'src/app/interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  username = null;
  password = null;

  constructor(
    private httpService: HttpService,
    private nav: NavController,
    public utilService: UtilService,
    public authService: AuthService
  ) { }

  ngOnInit() {
    var data =  this.authService.isAuthenticated();
    console.log(data);
    if (data != undefined && data != null) {
      this.nav.navigateRoot('');
    }
  }

  login(){
    if (this.username == null || this.username == ''){
    this.utilService.openSnackBar("請輸入用戶名稱");
    return;
  }
  if (this.password == null || this.password == ''){
    this.utilService.openSnackBar("請輸入密碼");
    return;
  }
  if (this.utilService.isLoading){
    return;
  }
  this.utilService.isLoading = true;
  this.authService.AdminLogin(this.username, this.password).then( res => {
    this.utilService.isLoading = false;
    console.log(res);
    if (res != null){
      // this.nav.navigateRoot('');
      window.location.href = '';
    }
    // else{
    //   // this.verify_code_field.setFocus();
    // }
  });
}

}
