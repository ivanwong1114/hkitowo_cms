import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Activity } from 'src/app/interface';
import { CsvService } from 'src/app/services/csv.service';
import { DataService } from 'src/app/services/data.service';
import { StateService } from 'src/app/services/state.service';
import { StorageService } from 'src/app/services/storage.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-activity-log',
  templateUrl: './activity-log.page.html',
  styleUrls: ['./activity-log.page.scss'],
})
export class ActivityLogPage implements OnInit {


  activity_data_list: Activity[] = null;

  displayedColumns: string[] = ['title', "create_datetime"];
  dataSource = new MatTableDataSource(null);

  @ViewChild(MatTable, { static: false }) table: MatTable<any>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private state: StateService,
    private storageService: StorageService,
    private cdr: ChangeDetectorRef,
    private dataService: DataService,
    private csvService: CsvService
  ) { }

  ngOnInit() {

    setTimeout(() => {

      console.log(this.dataService.getPartnerActivtyLog(this.state.activity_log_partner.value.id));
      this.initTable(this.dataService.getPartnerActivtyLog(this.state.activity_log_partner.value.id));
    }, 500);
  }

  initTable(data) {
    this.dataSource = new MatTableDataSource(data);
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.cdr.detectChanges();
    }, 300);

  }

}
