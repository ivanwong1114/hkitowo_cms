import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActivityLogPageRoutingModule } from './activity-log-routing.module';

import { ActivityLogPage } from './activity-log.page';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/app/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActivityLogPageRoutingModule,
    MaterialModule,
    SharedModule
  ],
  declarations: [ActivityLogPage]
})
export class ActivityLogPageModule {}
