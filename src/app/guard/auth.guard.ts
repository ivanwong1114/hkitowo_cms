import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from "@angular/router";
import { Observable } from "rxjs";
import { UtilService } from 'src/app/services/util.service';
import { HttpService } from 'src/app/services/http.service';
import { AuthService } from "../services/auth.service";
import { NavController } from "@ionic/angular";

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate {

  constructor(
    private httpService: HttpService,
    private utilService: UtilService,
    private router: Router,
    private authService: AuthService,
    private nav: NavController
  ){

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {

    let LoggedIn = this.authService.isAuthenticated();
    // let admin_data = this.authService.getAdminData();
    // console.log(token);
    // console.log(admin_data);
    if (LoggedIn == undefined || LoggedIn == null ){
      this.nav.navigateRoot('login');

      return false;
    }
    else{
      return true;
    }
  }
}
