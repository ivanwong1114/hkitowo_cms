export const environment = {
  production: true,
  isCMS: false,
  resetDatabaseOnInit: false
};
